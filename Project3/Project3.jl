#Project 3
#Cas Baskin
#Resistor Value Calculator

#     eeeeeee..eeeeee..eeeeee.eee..eeeeee.eeeeeeeee..eeeeee..eeeeeee..
#     @@@@@@@@:@@@@@@:@@@@@@@:@@@:@@@@@@@:@@@@@@@@@:@@@@@@@@:@@@@@@@@:
#     %%%--%%%-%%%----%%%-----%%%-%%%--------%%%----%%%--%%%-%%%--%%%-
#     &&&&&&&++&&&&&++&&&&&&++&&&+&&&&&&+++++&&&++++&&&++&&&+&&&&&&&++
#     ||||||***|||||***||||||*|||**||||||****|||****|||**|||*||||||***
#     !!!=!!!==!!!========!!!=!!!=====!!!====!!!====!!!==!!!=!!!=!!!==
#     :::##:::#::::::#:::::::#:::#:::::::####:::####::::::::#:::##:::#
#     ...@@...@......@......@@...@......@@@@@...@@@@@......@@...@@...@
#                                                                 
#             eee....eee..eeeeee..eee....eee..eee.eeeeee.
#             @@@::::@@@:@@@@@@@@:@@@::::@@@::@@@:@@@@@@:
#             %%%----%%%-%%%--%%%-%%%----%%%--%%%-%%%----
#             &&&++++&&&+&&&&&&&&+&&&++++&&&++&&&+&&&&&++
#             *|||**|||**||||||||*|||****|||**|||*|||||**
#             ==!!!!!!===!!!==!!!=!!!====!!!==!!!=!!!====
#             ###::::####:::##:::#::::::#::::::::#::::::#
#             @@@@..@@@@@...@@...@......@@......@@......@
#                                                
# .eeeee..eeeeee..eee.....eeeee.eee..eee.eee.....eeeeee..eeeeeeeee..eeeeee..eeeeeee..
# @@@@@@:@@@@@@@@:@@@::::@@@@@@:@@@::@@@:@@@::::@@@@@@@@:@@@@@@@@@:@@@@@@@@:@@@@@@@@:
# %%%----%%%--%%%-%%%----%%%----%%%--%%%-%%%----%%%--%%%----%%%----%%%--%%%-%%%--%%%-
# &&&++++&&&&&&&&+&&&++++&&&++++&&&++&&&+&&&++++&&&&&&&&++++&&&++++&&&++&&&+&&&&&&&++
# |||****||||||||*|||****|||****|||**|||*|||****||||||||****|||****|||**|||*||||||***
# !!!====!!!==!!!=!!!====!!!====!!!==!!!=!!!====!!!==!!!====!!!====!!!==!!!=!!!=!!!==
# ::::::#:::##:::#::::::#::::::#::::::::#::::::#:::##:::####:::####::::::::#:::##:::#
# @.....@...@@...@......@@.....@@......@@......@...@@...@@@@...@@@@@......@@...@@...@

                # So this is my FINAL project I'm pretty sure it turned
                # into some kind of mess. The assignment was to calculate
                # the resistance of a 4-band resistor based on the input
                # of the colors of the bands with the ultimate goal of
                # using dictionaries to find the values for each band. I
                # did that ==> however I have found that dictionaries are
                # kind of a pain when the order of the keys or values
                # matters to what you want to do. I realise that in the
                # newest versions of python dictionaries respect the
                # order of insertion but this is just a bonus added on to
                # the concept of the hash table which is the underlying
                # form of dictionaries. So, in this program while I did
                # in fact use a dictionary to get the values of each
                # bands color, I did so using Julia's built in
                # multidimensional arrays, with which I used Cartesian
                # indexing to get corresponding values. I'll try to
                # explain more when we get there.

                # This is an input function, there are many like it but
                # this one is mine.

function typeIn(prom::String):Any
    print(prom)
    w =chomp(readline())
    i=tryparse(Int64, w)
    f=tryparse(Float64, w)
    if typeof(f) ==Float64
        if typeof(i) == Int64
            return i
        end
    return f
    else
        return w
    end

end

                # This is a function that determines the number of digits
                # in the characteristic part (the integer part) of a 
                # fractional number. I'm pretty proud of this it seems to 
                # work pretty well. I haven't come up with a number yet 
                # that will throw it off.
function wholeN(num)
    x=num
    a=0
    while x>1
        x = x/10
        a += 1
    end
    return a
end

                # This one I am really proud of! This is a function that
                # determines the number of digits in the mantissa
                # (fractional part) of a fractional number. This
                # function, as well as pretty much all of this exercise
                # is all original. This is mostly due to the fact that
                # there is very little literature on Julia. I think I've
                # avoided all but some corner cases I haven't found yet
                # in bypassing the constraints of binary floating point
                # arithmetic.
function decN(num)
    x=num
    a=0
    while abs(x-round(x))>.01
        x = x*10
        a+=1
    end
    return a
end

                # So...
                # Julia has full support for "meta-programming" which is
                # a subject I am supremely interested in. So I decided to
                # try and do that. This is what I came up with I would
                # say the majority of it is entirely unnecessary but it
                # gave me some insight into how it works. This macro
                # returns an expression that generates a vector of integer 
                # floating point numbers in the (non exclusive) range of 
                # its two arguments.
macro n(n1,n2)
    return :([x*1.0 for x=$n1:$n2])
end

                # Julia uses a concept called multiple dispatch as
                # opposed to object methods (classes are simply user
                # defined types). In this system a function or macro can
                # be defined multiple times for different argument and
                # different types of arguments.
                # This macro creates an expression that creates a vector
                # of 10x multipliers based on a range (inclusive) of the 
                # arguments, and a step size.
macro mult(range1, step, range2)
    return :([10.0^x for x=$range1:$step:$range2])
end

                # This macro is the same as above but only takes range
                # arguments. They are both called the same way and the
                # macro is dispatched based on the number of arguments. I
                # could have also defined the types (such as Int64 or
                # Float64...) but I wanted to leave them free enough for
                # my re-use in the future.
macro mult(range1, range2)
    return :([10.0^x for x=$range1:$range2])
end

                # This one took a really long time to figure out. Julia
                # has a special type Nothing which only has a value of
                # nothing. I wanted a way to add this to my dictionary
                # lists to take the place of missing values. In
                # retrospect, and with a little mare research I probably
                # should have used a different Julia type Missing which
                # has special methods defined for it in many cases. But
                # it took me a little while to figure out how to define a
                # nothing Array that could, in the future be combined
                # with an array of different type values. That's what the
                # Union declaration is there for. So this macro returns
                # the expression to create such an array.
macro no(length)
    return :(Vector{Union{Float64, Nothing}}(nothing, $length))
end

                # This macro is completely unnecessary. It simply adds
                # the plus minus and percentage signs to the inputs and
                # adds then to a list. This is used for the tolerance
                # list. The ... in the arg section is called the "splat"
                # operator and it unpacks collections and iterators
                # inside a grouping such as () or []. In Julia you can
                # make a list/array from an iterator like [iter...]
                # that's pretty sweet.
macro per(arg...)
    x=[]
    for i in [arg...]
        push!(x,"±$i%")
    end
    return :($x)
end

                # This is basically the same result but with a
                # comprehension (which I really like despite them being
                # pretty slow compared to map and filter). This one is
                # for temperature tolerance for 6 band resistors.
macro tempCo(arg...)
    return :([string(x, " ppm/K") for x=[$(arg...)]])
end

                # This macro creates code that vectorizes all these other
                # macro created lists. This way I can fill all the gaps
                # of nonexistent values and have matching length vectors
                # in my dictionary.
macro catVec(var, macros...)
    eval(:($var = vcat($(macros...))))
end

                # This was another pretty tricky one. I wanted to have a
                # variable number of digit values in my dictionary due to
                # my overreach of wanting to be able to de multiple types
                # of resistors. So this macro creates a vector of symbols
                # (basically variables or expressions) that i can later
                # splat into the dictionary comprehension to get the
                # appropriate number of values for a square matrix later
                # on.
macro symbolicVec(var,N)
    return :([Symbol($var) for x=1:$N])
end

                # Using the cat macro to create the vector of colors.
@catVec colors "black" "brown" "red" "orange" "yellow" "green" "blue" "violet" "grey" "white" "gold" "silver"

                # language.
@catVec ords "first" "second" "third" "fourth" "fifth" "sixth"

                # values and some nothing at the end
@catVec vals @n(0,9) @no(2)

                # multipliers a little nothing and some more multipliers.
@catVec mult @mult(0,6) @no(3) @mult(-1,-1,-2)

                # tolerances! And nothing!
@catVec toll @no(1) @per(1,2) @no(2) @per(0.5,0.25,0.1) @no(2) @per(5,10)

                # see above!
@catVec temp @tempCo(250,100,50,15,25,20,10,5,1) @no(3)

                # I am also really proud of this. I realise I keep saying
                # that but I am amazed that I got everything I wanted to
                # do working somewhat. So this function takes a string
                # and a vector of strings to check it against. Makes a new
                # vector of regular expressions based on each entry.
                # Checks each regex vs the input string, and creates a
                # new vector of items from the check list with matches of
                # more than 3/4's it's length. This of course is not
                # the best, but it works pretty well and catches most
                # single typo's and what not. Of course if you enter 10
                # E's the list will be pretty long.
function glob(input, checkArray)
    regxVec = [Regex(string("[", join(split(checkArray[x], ""), ","), "]"), "i") for x=eachindex(checkArray)]
    matchVec=[]
    for x in regxVec
        push!(matchVec, collect(eachmatch(x, input)))
    end
    x=Dict([enumerate([checkArray[x] for x=eachindex(matchVec) if length(matchVec[x])>=length(checkArray[x])*3/4])...])
    y=hcat(sort([keys(x)...]), [x[i] for i=sort([keys(x)...])])
    return y
end

                # This is the main function. I intended to break it up
                # into many smaller functions, but the day grows short
                # and time is of the essence. Because of it's size I will
                # add comments in between important and not always
                # obvious actions. This first part sets up a loop so we
                # can do multiple operations. And says what it do'
function resistorCalculator()
    yn=1
    println("Resistor Calculator\nversion = .00001\n\nThis is a small program to calculate the resistance of a resistor with leads and color bands.")
    while yn != "q"
        while true
                # do we want to calculate resistance or band colors?
            global xIn=typeIn("First do you want to calculate the value of a resistor, or calculate the band colors for a resistor of a certain value?\n[1(ohms),2(bands)]>  ")
                # if everything went well
            if xIn in [1,2]
                xIn == 1 ? (global game="ohms") : (global game="bands")
                break
            end
                # if the user understands words better than numbers, or
                # has trouble with simple cues.
            if xIn in ["ohms", "bands"]
                xIn == "ohms" ? (global game = "ohms") : (global game = "bands")
                break
            end
                # if I wanted an excuse to use my glob function! This
                # code is reused all over this thing.
            if typeof(xIn) <: AbstractString
                gob = glob(xIn, ["ohms", "bands"])
                if length(gob) != 0
                    println("Did you mean,")
                    ind = size(gob)[1]
                # fancy print all the options with corresponding numbers.
                    for i in 1:ind
                        println("$(gob[i]) => $(gob[ind+i])")
                    end
                    println("If so please enter the corresponding number, or hit enter if you really messed it up that bad.")
                    xNum=typeIn("$(collect(1:ind))>  ")
                # if the number is good set game to the right value.
                    if xNum in collect(1:ind)
                        game=gob[indexin(xNum,gob[1:ind])[1],2]
                        break
                    end
                    if xNum == ""
                        continue
                    else
                # necessary roughness.
                        println("Really?")
                    end
                end
            else
                println("Ok let's try this again.")
            end
        end
                # This is the section defined by the project assignment,
                # mostly. This is for decoding band colors to get the
                # resistance.
        if game == "ohms" 
            while true
                # get the number of bands on the resistor, with some
                # input validation. I kinda just made everything global 
                # in this function because I still don't fully 
                # understand Julia's scoping rules.
                global resNum = typeIn("Please enter the number of bands on the resistor.\n[4,5,6]>  ")
                if resNum in [4,5,6]
                    break
                else
                    println("\nThis calculator only works on resisters with 4,5 or six bands.\n")
                end
            end
                # This creates the vector of Symbols that refer to the
                # vals list when evaluated. This is then used to create a
                # dictionary of arrays/lists/vectors whatever you want
                # to call it that have values ore nothing for each band,
                # with which I can make a square matrix and get the
                # diagonal stripe. Also the boolean ? something : other,
                # is a short form of an if statement in Julia.
            global symbolVec = @symbolicVec "vals" (resNum>=5 ? 3 : 2)
                # Create the dictionary I wanted to do this with quoted
                # code in a macro but, while I could make it work in the
                # REPL It didn't work the same in the script. The second
                # option adds the temperature tolerance.
            if resNum in [4,5]
                global Necronomicon = eval(:(Dict(i for i in zip(colors, [[x...] for x=zip($(symbolVec...),mult,toll)]))))
            else
                global Necronomicon = eval(:(Dict(i for i in zip(colors, [[x...] for x=zip($(symbolVec...),mult,toll,temp)]))))
            end
                # Life Hack!
            println("\nTypically the first band will be closer to the end of the resistor than the last one.\n")
                # Initialize an array for the input colors.
            global bandColors=[]
                # start a loop to append each input color to the array.
            for i in 1:resNum
                while true
                    l = length(bandColors)
                    ncolors = copy(colors)
                    filter!(x->Necronomicon[x][i]!=nothing, ncolors)
                    colr = typeIn("Please enter the color of the $(ords[l+1]) band.\n$(ncolors)\n>  ")
                    if colr in ncolors
                        push!(bandColors, colr)
                        break
                    end
                # More pseudo globing! With insults!  I really am pretty 
                # happy with this, it gets a little weird with really 
                # long words and really short words, but it seems to do 
                # a pretty good job, case insensitive! Also my overuse 
                # of => is due to the Fira font which has nice code 
                # ligatures so it looks super 133t.
                    if typeof(colr) <: AbstractString
                        gob = glob(colr, ncolors)
                        if length(gob) != 0
                            println("Perhaps you meant,")
                            ind = size(gob)[1]
                            for i=1:ind
                                println("$(gob[i]) => $(gob[ind+i])")
                            end
                            colorNum = typeIn("$(collect(1:ind))>  ")
                            if colorNum in collect(1:ind)
                                colr=gob[indexin(colorNum,gob[1:ind])[1],2]
                                push!(bandColors, colr)
                                break
                            end
                        end
                        println("I guess we can try this one more time...\n")
                    else
                        println("Really? I mean what are we even doing here? sheeze")
                    end
                end
            end
                # This is another pretty sweet use of the splat operator
                # to avoid a nested comprehension. This creates a square
                # matrix in which the principal diagonal is the vector of
                # values for the resistor. 
            decode = hcat([Necronomicon[bandColors[x]] for x=1:length(bandColors)]...)
            if resNum in [5,6]
                # This makes the new array from the primary diagonal.
                rV = [x==1 ? decode[x,x]*100 : x==2 ? decode[x,x]*10 : decode[x,x] for x=1:size(decode)[1]]
                # This formats the final values into a string including
                # the tolerance and the temperature tolerance. Two
                # choices for 5 or 6
                if resNum == 6
                    global final = string(round(((rV[1] + rV[2] + rV[3]) * rV[4]), digits=2), "Ω ohms ", rV[5], " with a change in resistance of ", rV[6])
                else
                    global final = string(round(((rV[1] + rV[2] + rV[3]) * rV[4]), digits=2), "Ω ohms ", rV[5])
                end
            else
                # The same for 4 bands.
                rV = [x==1 ? decode[x,x]*10 : decode[x,x] for x=1:size(decode)[1]]
                global final = string((rV[1] + rV[2]) * rV[3], "Ω ohms ", rV[4])
            end
                # tell em the damn thing.
            println("The total resistance of your $(length(bandColors)) resistor with color bands of,")
            for i in 1:length(bandColors)
                sleep(.5)
                println(bandColors[i])
            end
            println("is, $final")    
        end
                # This is the unauthorized section. I really wanted to
                # try and go the other way, I had originally conceived
                # an algorithm that could calculate the values of
                # multiple resistors based on complex input and had a few
                # working functions to facilitate it, but time was
                # pressing so I stuck with five band resistors which seem
                # to be the most common these days. Although if you're
                # working with older resistors you could use this to
                # calculate the first two bands and recalculate the last
                # number and subtract a black band. Maybe that's too
                # complicated.
        if game == "bands"
                # time
            global resNum = 5
            global symbolVec = @symbolicVec "vals" (resNum>=5 ? 3 : 2)
            global Necronomicon = eval(:(Dict(i for i in zip(colors, [[x...] for x=zip($(symbolVec...),mult,toll)]))))
                # The main input loop to get the desired resistance. Gets
                # the whole number part and fractional part to determine
                # if the largest sigfig is within the limits of the
                # largest multiplier, and how many sig figs are in the
                # fractional part. This is all with the goal of figuring
                # out the best way to truncate the number to the three
                # largest sigfigs in the number and place them in the
                # hundreds tens and ones places and record the
                # multiplier in a variable.
            while true
                global resist = typeIn("Please enter the desired resistance, greater than .01 and less than 9.99 × 10^8 ohms.\nWith three significant digits next to eachother please.\nIf you enter something with more digits they will be truncated to three.\n[0-9.99e6]>  ")
                if typeof(resist) <: Number && 0<resist<9.99e8
                    global whole = wholeN(resist)   
                    global dec = decN(resist)
                    global num
                    if 9 > whole >= 3
                        global num = resist÷mult[whole-2]
                        global plier = mult[whole-2]
                        break
                    end
                    if whole > 9
                        println("\nLets try to keep the multiplier under 1 MΩ.")
                    end
                    if whole == 2
                        global num = (resist*10)÷1
                        global plier = mult[11]
                        break
                    end
                    if whole == 1
                        global num = (resist*100)÷1
                        global plier = mult[12]
                        break
                    end
                    if whole == 0 && div != 0
                        global num = resist*100÷1
                        global plier = mult[12]
                        break
                    end
                else
                    println("\nHere we go again, please try and follow the very simple instructions.")
                end
            end
                # Clean it up.
            global num = Int64(num)
                # Easter egg
            if num == 0
                global colorVec = split("invisible,"^4*"invisible",",")
            else
                # array of digits in the rounded final sigfigs
                global intVec = [parse(Int64, x) for x=split(string(num), "")]
                for i=1:(3-length(intVec))
                    pushfirst!(intVec, 0)
                end
                # new vector that replaces those numbers with their
                # corresponding colors from the color list. 
                global colorVec = [colors[i+1] for i=intVec]
                push!(colorVec, Dict(x for x=zip(mult,colors))[plier])
            end
                # get desired tolerance.
            while true
                println("\nNow enter your desired tolerance from the following list.\nIf there is no specified tolerance press enter/return\nfor the default tolerance ±5%.\n")
                # This part is kinda convoluted, it uses a list of the
                # tolerances for selection with a default of ±5%.
                toleranceVector = [x for x=toll if x != nothing]
                ind = length(toleranceVector)
                for i=1:ind
                    println("$i => $(toleranceVector[i])")
                end
                tolNum = typeIn("[1-7]>  ")
                if tolNum in 1:ind
                    global tolerance = toleranceVector[tolNum]
                    break
                elseif tolNum == ""
                    global tolerance = "±0.5%"
                    break
                else
                    print("\nAll you have to do is enter one of the identifying numbers.\n")
                end
            end
            push!(colorVec, Dict(x for x=zip(toll,colors))[tolerance])
            # tell the thing
            println("For your selected resistor of $(num * plier)Ω ohms with a tolerance of ", tolerance, "\n")
            for i in 1:5
                sleep(.5)
                println("The color of the $(ords[i]) band is $(colorVec[i]).")
            end
        end
            # Ask if there are more questions.
        yn=typeIn("Would you like to calculate another value, or quit?\n[y,n,q]>  ")
        if yn in ["y"]
            continue
        elseif yn in ["n", "q"]
            break
        else
            println("Is this going to be athing or something? I mean ti's really not that hard.")
        end
    end

end

resistorCalculator()

#for x in eachindex(matchlist)
#if length(matchlist[x])>=length(colors[x])*3/4
#println(colors[x])
#end
#end

# eval(:(Dict(i for i in zip(colors, [[x...] for x=zip($(values...),mult,toll)]))))
# k=[match(r"[b,l,a,c,k]"i, string(x)) for x in "Blagk"]
# m=eachmatch(r"(?i)[b,l,a,c,k]", "Blagk")
# re=Regex(string("[",join(split(colors[1], ""), ","),"]"))
# k=[typeof(x)!=Nothing ? x.match : nothing for x=k]
