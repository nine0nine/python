#Caspian Baskin
#EGR 115
#HW7
#Some time in March

                    # I kinda rushed this one a bit I don't think there
                    # are any bugs though. I did read ahead some and I
                    # was going to try to use some of the object oriented
                    # aspects of python but honestly the more I look at
                    # it the more annoyed and confused by it I become.
                    # Functions for life!!! Import sleep for drama, 
                    # import OrderedDict mostly just so I can reuse some
                    # older code and import secrets to use the os's most
                    # cryptographically secure random number generator as
                    # opposed to the less secure random module
                    # implementation.
from time import sleep
from collections import OrderedDict
import secrets

                    # This is a function that loops an input question 
                    # until an appropriate number is entered
def numLoop(_prom, _insult):
    while True:
        x = typeIn(_prom)
        if type(x) == int or type(x) == float:
            return x
            break
        elif x == "":
            return x
            break
        else:
            print(_insult)
                    # The same as the previous function but for a string.
                    # I changed it a bit to restrict valid strings to
                    # those present in a list argument.
def strLoop(_prom, _insult, _list):
    while True:
        x = typeIn(_prom)
        if type(x) == str and x in _list:
            return x
            break
        else:
            print(_insult)
                    # This is a redefined input function that types the
                    # input string. Why this isn't built in is beyond me
                    # and I'm sure the answer involves the words
                    # "readable" and "pythonic", blegh.
def typeIn(promt):
    stuff = input(promt)
    try:
        stuff = float(stuff)
        if stuff%1 == 0:
            stuff = int(stuff)
        return stuff
    except ValueError:
        return stuff
                    # Big long nasty prompt list, I like the final look
                    # of this better than having huge print statements
                    # everywhere, but I think next time it's getting a
                    # short name like _p or something. Also it would be
                    # great if we could use a couple files one being a
                    # module file or something. I dunno maybe It's not
                    # necessary.
prmpts = ["This program will ", 
          "add up daily sales for the week.",
          "Please enter the total daily sales for",
          "[$]>  ",
          "With sales of ",
          "Total sales for the week are, ",
          "generate 7 random (and cryptographically secure!) lotto numbers.",
          "Give you some rainfall statistics!",
          "Please enter the average rainfall for",
          "[inches]>  ",
          "accept;  \nA manually entered list of numbers or, \nA linear range of numbers along with a step/slope number, \nand a number that represents the highest value to remove from the list, \nthen print all the input numbers greater than the exclusion number.",
          "Would you like to use a linear list of numbers along with a slope, or manually enter in a list of numbers?",
          "[man, line]>  ",
          "Please enter the lowest number for your list.",
          "[number]>  ",
          "Please enter the highest number of the list.",
          "Now enter the slope of the list.",
          "Please enter the",
          "number in your list, or hit enter to finish",
          "Please enter the next number in your list, or enter to finish.",
          "[number, enter]>  ",
          "Please enter the highest number to remove from the list.",
          "The list of numbers greater than,",
          "is,"
          ]
                    # I know I'm really slacking on my insults. Maybe
                    # that's what a module file would be good for! Import
                    # insults!
insults = ["It's really got to be a number here people.", 
           """Either type "man" or "line", case sensitive, or enter to finish."""
           ]
                    # Dictionary with language as keys and months as
                    # values. I don't really like it, but it was already
                    # made and I was behind. I gotta figure out a python
                    # database for all this language I keep insisting on
                    # using.
months = OrderedDict([('first', 'January'),
                      ('second', 'February'),
                      ('third', 'March'),
                      ('fourth', 'April'),
                      ('fifth', 'May'),
                      ('sixth', 'June'),
                      ('seventh', 'July'),
                      ('eight', 'August'),
                      ('ninth', 'September'),
                      ('tenth', 'October'),
                      ('eleventh', 'November'),
                      ('twelfth', 'December')])
         
                    # More of that only days this time!
days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
                    # clear screen function, I like it but IDLE sucks all
                    # the fun out of it. As soon as I figure out how to
                    # bypass IDLE's autofold function in the program
                    # it'll be back. mwahahahahahaaaaa!
def clear_screen():
    print("\n" * 150)
                    # generator that does what range should do. WARNING
                    # may not be "readable" or "pythonic". Takes two
                    # numbers and a step range and makes an iterator with
                    # all between values. I fixed the weird floating
                    # point stuff in a different function this time, but
                    # I'll probably move it inside this function for next
                    # time. Ooh also the final number is inclusive crazy
                    # right?
def drange(low, high, step):
    _low = low
    while True:
        if _low > high:
            return
        yield _low
        _low += step
                    # Take an input in inches convert it to feet and
                    # inches, and return a string with each unless they
                    # are 0. Also includes units in the string.
def inFt(inches):
    feet = inches//12
    inch = inches%12
    if feet == 0:
        feet = ""
    else:
        feet = (str(feet) + " feet ")
    if inch == 0:
        inch = ""
    else:
        inch = (str(inch) + " inches")
    return (feet + inch)
                    # Function that creates a list of numbers from user
                    # input. Prompt re-uses language from month dict.
                    # empty string cancels further input IE, pressing
                    # enter.
def manList():
    _manList = []
    S = 0
    while "" not in _manList:
        if S <= 11:
            _manList.append(numLoop(("\n" + prmpts[17] + " " + list(months.keys())[S] + " " + prmpts[18] + "\n" + prmpts[20]), insults[0]))
        else:
             _manList.append(numLoop(("\n" + prmpts[19] + "\n" + prmpts[20]), insults[0]))
        S += 1
    return _manList
                    # Uses the decimal range function (drange) to create
                    # a list from user input start and end values and 
                    # rounds crazy floating point baloney based on the
                    # precision of the step value.
def linearList():
    low = numLoop(("\n" + prmpts[13] + "\n" + prmpts[14]), insults[0])
    high = numLoop(("\n" + prmpts[15] + "\n" + prmpts[14]), insults[0]) 
    step = numLoop(("\n" + prmpts[16] + "\n" + prmpts[14]), insults[0])
    try:
        return  [round(x, len(str(step).split(".")[1]))for x in drange(low, high, step)]
    except IndexError:
        return [x for x in drange(low, high, step)]
    
                    # Function that removes all values equal to or less
                    # than the specified split value. Also removes the
                    # empty string left over from the manual list
                    # function (manList) and sorts the list. I'm still
                    # not sure why I couldn't just do the removing part
                    # with a for loop only but it's probably because it
                    # was changing the iterator. I bet if i used the
                    # input list it would work.
def gtList(_min,_list):
    _x = _list
    for i in _list:
        if i == "":
            _x.remove(i)
    _list.sort()
    while True:
        for i in _x:
            if i <= _min:
                _x.remove(i)
        if _x[0] > _min:
            return _x
            break

                    # Main function for example 1. Explain what it does.
                    # Then ask for input in $ format for each day of the
                    # week and append it to a list. The assignment said I
                    # had to use a loop to calculate the total so I did.
                    # Ha! Then show all the input info and then the
                    # total.
def ex_1():
    print(prmpts[0], end="")
    print(prmpts[1])
    sales = []
    for day in days:
        print("\n\n", prmpts[2], day)
        x = numLoop(prmpts[3], insults[0])
        sales.append(x)
    for i in [1]:
        tot = sum(sales)
    print(prmpts[4])
    for day in days:
        print("\n", day, "\r\t\t\t\t$", format(sales[days.index(day)], ',.2f'), sep="")
        sleep(.5)
    print("\n", prmpts[5])
    print("$", format(tot, ',.2f'))
                    # Main function for exercise 2. Explain. Generate a
                    # list of acceptable numbers. Then use secrets module
                    # to generate random numbers from the list. The
                    # secrets module supposedly uses os module stuff to
                    # find a secure PRNG in the host's operating system
                    # so this should work fine as I'm sure windows and
                    # mac should have some sort of SSL or public key
                    # generator. Then print it all fancy like. I wanted
                    # to have the password part centered but it was too
                    # late to get it done.
def ex_2():
    print("\n")
    a = list(range(10))
    lotto = []
    for i in range(7):
        lotto.append(secrets.choice(a))
    print(prmpts[0], prmpts[6])
    print("\n\n")
    print("*  *  *  *  *  *  *\r", end="")
    for i in lotto:
        sleep(1)
        print(i, " ", flush=True, end="")
    sleep(1)
                    # Main func for example 3. Explain. Repeat first part
                    # of example 1 only this time with rain in inches
                    # instead of money. Then get the month string from
                    # the dict for the largest rainfall month and the
                    # least. Then print the max and min rain values after
                    # converting to fancy feet and inches. Then print the
                    # total for the year.
def ex_3():
    print("\n", prmpts[0], prmpts[7])
    rain = []
    for i in list(months.keys()):
        print("\n", prmpts[8], months[i], "in inches.\n\n")
        rain.append(numLoop(prmpts[9], insults[0]))
    maxMon = list(months.values())[rain.index(max(rain))]
    minMon = list(months.values())[rain.index(min(rain))]
    print("The maximum rainfall occurred in", maxMon)
    print(inFt(max(rain)))
    print("The minimum rainfall occurred in", minMon)
    print(inFt(min(rain)))
    print("The total rainfall for the year was", inFt(sum(rain)))
                    # Main ex 6. Explain! "The Decider" determines if 
                    # the user wants a linear or manually entered list of
                    # numbers. Then run the appropriate function to
                    # generate the list. Then get the split number from
                    # the user and create a new list with only numbers higher
                    # than it from the number list. Then spit it out.
def ex_6():
    print(prmpts[0], prmpts[10])
    sleep(2)
    theDecider = strLoop("\n" + prmpts[11] + "\n" + prmpts[12], insults[1], ["man", "line"])
    if theDecider == "man":
        thisList = manList()
    if theDecider == "line":
        thisList = linearList()
    _min = numLoop("\n" + prmpts[21] + "\n" + prmpts[14], insults[0])
    thatList = gtList(_min, thisList)
    sleep(.7)
    print("\n", prmpts[22], _min, prmpts[23] )
    sleep(.7)
    print(thatList)

                    # Copy paste from other assignment
prawnz = ["\nPlease select which programming exercise you would like to run, or q to exit:\n[1,2,3,6 or q]>  "]  

                    # This is the main loop. It asks for input of the
                    # exercise number and then runs the corresponding
                    # function. It will continue looping until the letter
                    # "q" is entered.

def mainList():
    ex_num = 'foo' 
    while ex_num != 'q': 
        print("\n")
        ex_num = typeIn(prawnz[0])
        if ex_num == 1:
            ex_1()
        elif ex_num == 2:
            ex_2()
        elif ex_num == 3:
            ex_3()
        elif ex_num == 6:
            ex_6()

                    # run that puppy!
mainList()

































                    # such a empty! No jokes for you!
