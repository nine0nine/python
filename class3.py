def hiFunction():
    print("Hi EGR115, we're using a void function.")

hiFunction()

def areaCircle( rad ):
    area = 3.1415 * rad **2
    return area

print("Area of circle with radius 3 is ", areaCircle(3))
