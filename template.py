#Caspian Baskin
#EGR 115
#HW7
#Some time in March

from time import sleep
from collections import OrderedDict
import secrets

def numLoop(_prom, _insult):
    while True:
        x = typeIn(_prom)
        if type(x) == int or type(x) == float:
            return x
            break
        elif x == "":
            return x
            break
        else:
            print(_insult)

def strLoop(_prom, _insult, _list):
    while True:
        x = typeIn(_prom)
        if type(x) == str and x in _list:
            return x
            break
        else:
            print(_insult)

def typeIn(promt):
    stuff = input(promt)
    try:
        stuff = float(stuff)
        if stuff%1 == 0:
            stuff = int(stuff)
        return stuff
    except ValueError:
        return stuff

prmpts = ["This program will ", 
          "add up daily sales for the week.",
          "Please enter the total daily sales for",
          "[$]>  ",
          "With sales of ",
          "Total sales for the week are, ",
          "generate 7 random (and cryptographically secure!) lotto numbers.",
          "Give you some rainfall statistics!",
          "Please enter the average rainfall for",
          "[inches]>  ",
          "accept;  \nA manually entered list of numbers or, \nA linear range of numbers along with a step/slope number, \nand a number that represents the highest value to remove from the list, \nthen print all the input numbers greater than the exclusion number.",
          "Would you like to use a linear list of numbers along with a slope, or manually enter in a list of numbers?",
          "[man, line]>  ",
          "Please enter the lowest number for your list.",
          "[number]>  ",
          "Please enter the highest number of the list.",
          "Now enter the slope of the list.",
          "Please enter the",
          "number in your list, or hit enter to finish",
          "Please enter the next number in your list, or enter to finish.",
          "[number, enter]>  ",
          "Please enter the highest number to remove from the list.",
          "The list of numbers greater than,",
          "is,"
          ]
                    # I know I'm really slacking on my insults. Maybe
                    # that's what a module file would be good for! Import
                    # insults!
insults = ["It's really got to be a number here people.", 
           """Either type "man" or "line", case sensitive, or enter to finish."""
           ]
                    # Dictionary with language as keys and months as
                    # values. I don't really like it, but it was already
                    # made and I was behind. I gotta figure out a python
                    # database for all this language I keep insisting on
                    # using.
months = OrderedDict([('first', 'January'),
                      ('second', 'February'),
                      ('third', 'March'),
                      ('fourth', 'April'),
                      ('fifth', 'May'),
                      ('sixth', 'June'),
                      ('seventh', 'July'),
                      ('eight', 'August'),
                      ('ninth', 'September'),
                      ('tenth', 'October'),
                      ('eleventh', 'November'),
                      ('twelfth', 'December')])
         
                    # More of that only days this time!
days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

def clear_screen():
    print("\n" * 150)

def drange(low, high, step):
    _low = low
    while True:
        if _low > high:
            return
        yield _low
        _low += step

                    # Copy paste from other assignment
prawnz = ["\nPlease select which programming exercise you would like to run, or q to exit:\n[1,2,3,6 or q]>  "]  

                    # This is the main loop. It asks for input of the
                    # exercise number and then runs the corresponding
                    # function. It will continue looping until the letter
                    # "q" is entered.

def mainList():
    ex_num = 'foo' 
    while ex_num != 'q': 
        print("\n")
        ex_num = typeIn(prawnz[0])
        if ex_num == 1:
            ex_1()
        elif ex_num == 2:
            ex_2()
        elif ex_num == 3:
            ex_3()
        elif ex_num == 6:
            ex_6()

                    # run that puppy!
mainList()

































                    # such a empty! No jokes for you!
