from time import sleep
nRows = int(input("Enter the number of rows "))
nColumns = int(input("Enter the number of columns "))
counter = 0

for x in range(nRows):
    for y in range(nColumns):
        counter += 1
        print("\t", counter, flush=True, end="")
        sleep(.3)
    print("", flush=True)
