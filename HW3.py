#Caspian Baskin
#EGR 115
#HW1
#Jan,28,2019







#Initialise list structure. 
#I read ahead a bit. I have some limited experience with programming 
#languages and figured this structure would be useful throughout the class
#let me know if it's inappropriate
import time #ensure the user will spend the longest time with my programs
import random #introduce some simulated chaos

prompt = "Please select which programming exercise you would like to run 1,2,4,6,7,10 or q to quit\n>  " #Prompt for main HW list
ex_num = 'foo' #Initialize list input variable to something other than the value in while statement
while ex_num != 'q': #start main list loop
    print("\n")                     
    ex_num = input(prompt) #ask for main list input


# A program to calculate the total number of bugz collected by a bug collector in a five day period
    if ex_num == '1':  
        bug_prompt = "\n\nPlease enter the number of bugz collected on " #prompt text
        dayz = [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ] #Dayz of the week
        total_bugz = 0 #initialize accumulator
        airoar = True #guilty until proven innocent
        while airoar == True: #the trial
            try: #ask for input and calculate total if the user inputs numbers
                for i in range(5): #start a loop
                    bugz = int(input(bug_prompt + dayz[i] + "\n>  ")) #ask for the number of bugz for each day
                    total_bugz = total_bugz + bugz #update accumulator
                airoar = False #deny any wrongdoing
            except ValueError: #prosecution?
                print("\n\nA number written with numbers please.\n") #passive aggressive error
                time.sleep(2) #allow suspense to build
                airoar = True #guilty!
        if total_bugz > 100: #check to see if the bug collector is a professional or not
            print("\nThis week you collected", total_bugz, "bugz. That's a lot, you should probably take a break now.") # a compliment
        else: #if bug_collector not in professionals:
            print("\nThis week you collected", total_bugz, "bugz!!!") #show the total


# A program to calculate the number of calories burned on a treadmill
    elif ex_num == '2': 
        x=10 # floor of reasonable values
        burn_time = "foo" #initialize awesome variable
        trainer = ["Can you feel it?", "Keep pushin!", "Yeah!", "Get there!", "You're a natural!", "Whatcha waitin for? an invitation?", "Harder, better, faster, stronger!", "Burn baby burn!"] # initialize a trainer with helpful comments
        burn_time = input("How many minutes do you think you can actually stay on that treadmill?\n>  ") # ask for the time the user believes they can run the treadmill
        while type(burn_time) == str: #value error loop written after program so doesn't quite match the rest
            try:
                burn_time = float(burn_time) #see if its a number
                if ( burn_time % 5 ) > 2.5: #round up the number to a multiple of five
                    burn_time = burn_time + 5
                    burn_time = (burn_time // 5) * 5
                else:
                    burn_time = (burn_time // 5) * 5
            except ValueError: #check for user error
                burn_time = input("C'mon let's get serious' it's time to feel the burn.\n>  ") #insult
        while x <= burn_time: #start output loop
            print("\nYou gonna burn", x * 4.2, "calories, after", x, "minutes.") #output
            print("\n", trainer[random.randint(0,7)], "\n") #random encouragement
            x = x + 5 #increment
            time.sleep(2) #nap
        if burn_time < 10: #check for laziness
            print("\nForget that, come back when you're ready to burn some calories. Fool!") #insult and name calling



# A Program that shows distance travelled based on speed and time
    elif ex_num == '4': 
        airoar = True #guilty
        while airoar == True: #trial
            try: #defense
                speid = float(input("\nWhat is the speed of the vehicle in mph?\n>  ")) #request speed
                tim = float(input("\nHow many hours has it traveled?\n>  ")) #request time
                print("\n\nProcessing .", end='') #begin really old computer emulation
                if tim < 10: #initialize a reasonable old computer delay based on input
                    cray = tim
                else:
                    cray = 10
                i=1 #variable so i can use while instead of for, because
                while i in range(0,int(cray)*10,1): #emulate slow processing power
                    time.sleep(0.1) 
                    print(".", flush=True, end='') #useless progress bar
                    i = i + 1
                print("\n\n\nHour\t\tDistance traveled") #header
                for i in range(1,round( tim ) + 1,1): #calculate and print the actual answer in milliseconds with potentially buggy formatting
                    print(" ", i, "\t\t", i * speid) #answers
                airoar = False #not guilty
            except ValueError: #prosecution
                print("\n\nNumbers are typically found on the upper portion of your keyboard.") #insult
                time.sleep(1) #let the insult sink in
                airoar = True #guilty

   
    elif ex_num == '6':
        print("+---------+---------+\n", "|", '{:^9}'.format("F"), "|", '{:^9}'.format("C"), "|", sep="") #probably buggy ASCII table "top"
        for C in range(21): #loop
            print("|", '{:^7.1f}'.format(9/5*C+32),"|",  '{:^7.1f}'.format(C), "|") #calculations and answers in a hopefully more portable format
        print("+---------+---------+") #see above
        
        def drange(a,b,c): #define a new range function that accepts decimal steps (supposedly how the range function is implemented, ~google)
            x = a 
            while True: #check to see if the programmer is an idiot
                if x >= b:return #if so, do nothing
                yield x #cough up a value
                x += c #increment

        print("\n\n\n") #spaaaaace....
        for C in drange(1,20.1,.1): #start awesome counter
            print('{:^17}'.format(str(format(9/5*C+32, '.1f')) +"F"),"|", '{:^13}'.format(str(format(C, '.1f')) + "C"), end="\r", flush=True) #print a (hopefully) increasing display of C vs F
            time.sleep(.09) #not too fast not too slow make smaller if in a hurry


#Program to calculate totals for a doubling geometric series for pennies.
    elif ex_num == '7':
        airoar = True #guilty
        while airoar == True: #trial
            try: #defence
                daze = int(input("\nImagine you've been offered a job. With an increasing pay scale that doubles every day\n(I know nice right?). The first day is a penny, the second two, and each day it doubles.\nEnter the number of days you would like to work this job.\n>  ")) #explain and ask for a number of days
                for i in range(daze): #loop to calculate each days pay
                    pae = 1*2**i #formula
                    print("\nYou're going to make $", format(pae/100, ',.2f'), ", on day ", i+1, sep='') #output
                    time.sleep(.5) #dramatic pause
                print("\nYour total pay is, $", format((2**daze-1)/100, ',.2f'), "...", sep='') #calculate and output total
                time.sleep(2) #more drama
                print("\nnice.") #very dramatic
                time.sleep(2) #time to think
                airoar = False #not guilty
            except ValueError: #prosecution
                print("\nReally? ...\n") #insult
                time.sleep(2) #drama
                print("\nThis is a serious program, and you're typing nonsense. sheeeze!\n") #insult/drama
                time.sleep(2) #time to think
                airoar = True #guilty


# Estimate tuition increases for the next five years
    elif ex_num == '10':
        def growth(x0,r,t): #growth function; x0 = initial value, r = growth rate, t = time
            global growed # output variable
            growed = x0*(1+r)**t #formulae
        x0 = 8000.0 #initial value
        r = 0.03 #growth rate
        for i in range(6): #start loop for next 5 yrs
            growth(x0,r,i) #apply fancy function
            if i < 1: #needed for proper English
                print("\nStarting tuition is $", format(growed, ',.2f'), sep='') #show the starting tuition
            if i == 1: #English
                thstnd = "st" #postfix variable
            if i == 2:
                thstnd = "nd"
            if i in range(4,6):
                thstnd = "th"
            if i != 0: #print the calculated tuition with gud speak
                print("\nThe ", i, thstnd, " year of increase, the projected tuition will be, $", format(growed, ',.2f'), sep='')
            time.sleep(.5) #drama



