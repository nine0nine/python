#length conversion program
#Cas Baskin
#ENG 112
orig_leng = float(input("""Input the length you wish to convert """))                                            #get length and units and desired units
orig_unit = input("""What units are you converting from: inch, foot or meter? """)
fin_unit = input("""What units are you converting to: inch, foot or meter? """)
ItoM = (2.54/100)
IinF = 12.0
if orig_unit == 'inch':
    if fin_unit == 'foot': 
        fin_leng = orig_leng / IinF
    if fin_unit == 'meter':
        fin_leng = orig_leng * ItoM

elif orig_unit == 'foot':
    if fin_unit == 'inch':
        fin_leng = orig_leng * IinF
    if fin_unit == 'meter':
        fin_leng = orig_leng * IinF * ItoM

elif orig_unit == 'meter':
    if fin_unit == 'inch':
        fin_leng = orig_leng  / ItoM
    if fin_unit == 'foot':
        fin_leng = orig_leng / IinF / ItoM

else:
    print("""KERNEL_PANIC: entered units invalid""")

print(orig_leng, orig_unit, """is equal to""", format(fin_leng, '.2f'), fin_unit) 
