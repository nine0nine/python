#Caspian Baskin
#EGR 115
#HW7
#Some time in March

from time import sleep
from collections import OrderedDict
import random
import os
import re
from string import ascii_lowercase as letters
from string import ascii_uppercase as LETTERS
from string import printable as printable
                    # The range function the way I wish it was. Defaults
                    # to starting at 1 and includes the high number. the
                    # only problem is the syntax for other than defaults is a
                    # little funny.
def drange(high, low=1, step=1):
    _low = low
    while True:
        if _low > high:
            return
        yield _low
        _low += step
                    # AAAAaaaaand a numerate function that behaves the
                    # way I want it to by starting at 1 and including the
                    # final number.
def numberate(aaargh):
    for x in zip(drange(len(aaargh)), aaargh):
        yield x
                    # The same as always
def numLoop(_prom, _insult):
    while True:
        x = typeIn(_prom)
        if type(x) == int or type(x) == float:
            return x
            break
        elif x == "":
            return x
            break
        else:
            print(_insult)
                    # Also the same as it has been
def strLoop(_prom, _insult, _list):
    while True:
        x = typeIn(_prom)
        if type(x) == str and x in _list:
            return x
            break
        else:
            print(_insult)
                    # my input function
def typeIn(promt):
    stuff = input(promt)
    try:
        stuff = float(stuff)
        if stuff%1 == 0:
            stuff = int(stuff)
        return stuff
    except ValueError:
        return stuff
                    # Extra short prompt list. I was trying to knock this
                    # assignment out quick and then i got the idea to do
                    # a more complicated cipher which ended up taking a
                    # whole day almost to figure out. but i Think it
                    # works pretty good.
prompt = [ "Please enter a course number.",
            "Wanna play a game!",
          ]
                    # Dictionaries! and more Dictionaries! The first one
                    # just makes a dictionary from two lists. The second
                    # function creates the class names with a clever list
                    # comprehension and list flattening built in!. The
                    # third makes four lists out of three lists! i wish
                    # we had arrays :( the main function builds the dict
                    # with class crn's as keys and list of attributes for
                    # each class as values. then prints out the stuff.
def genDict(crn, info):
    return dict(zip(crn, info))
def crn():
    return [i for s in [["CS10" +str(i) for i in range(1,4)], ["NT110", "CM241"]] for i in s]
def info():
    a=[3004, 4501, 6755, 1244, 1411]
    b=["Haynes", "Alvarado", "Rich", "Burke", "Lee"]
    c=["8:00 am", "9:00 am", "10:00 am", "11:00 am", "1:00 pm"]
    return [list(i) for i in list(zip(a,b,c))]
def ex_1():
    x = genDict(crn(),info())
    print(prompt[0])
    y = strLoop(str(crn()) + ">  ", "\nIt needs to be one of,", list(crn()))
    sleep(.5)
    print("\nYour room number is", x[y][0])
    sleep(.5)
    print("\nYour teachers name is,", x[y][1])
    sleep(.5)
    print("\nThe class starts at,", x[y][2])
    sleep(1)
                    # This one makes a dict just like the book says, It's
                    # kinda boring.
def statesDict():
    states = """Alabama:Montgomery
                Alaska:Juneau
                Arizona:Phoenix
                Arkansas:Little Rock
                California:Sacramento
                Colorado:Denver
                Connecticut:Hartford
                Delaware:Dover
                Florida:Tallahassee
                Georgia:Atlanta
                Hawaii:Honolulu
                Idaho:Boise
                Illinois:Springfield
                Indiana:Indianapolis
                Iowa:Des Moines
                Kansas:Topeka
                Kentucky:Frankfort
                Louisiana:Baton Rouge
                Maine:Augusta
                Maryland:Annapolis
                Massachusetts:Boston
                Michigan:Lansing
                Minnesota:Saint Paul
                Mississippi:Jackson
                Missouri:Jefferson City
                Montana:Helena
                Nebraska:Lincoln
                Nevada:Carson City
                New Hampshire:Concord
                New Jersey:Trenton
                New Mexico:Santa Fe
                New York:Albany
                North Carolina:Raleigh
                North Dakota:Bismarck
                Ohio:Columbus
                Oklahoma:Oklahoma City
                Oregon:Salem
                Pennsylvania:Harrisburg
                Rhode Island:Providence
                South Carolina:Columbia
                South Dakota:Pierre
                Tennessee:Nashville
                Texas:Austin
                Utah:Salt Lake City
                Vermont:Montpelier
                Virginia:Richmond
                Washington:Olympia
                West Virginia:Charleston
                Wisconsin:Madison
                Wyoming:Cheyenne"""
    return dict([i.split(":") for i in [i.strip() for i in states.split("\n")]])
                    # The main function here just asks the user if they
                    # want to play then does the thing. I don't really
                    # like the fact that you have to enter in the name of
                    # the capitol exactly, and thought about adding
                    # regex's to make the basic letters themselves match
                    # but blegh. So It also went duplicate itself withouh
                    # asking if you want to play again and i dunno.
def ex_2():
    states = statesDict()
    triv = list(states.keys())
    random.shuffle(triv)
    while True:
        print(prompt[1])
        yn = strLoop("[y/n]>  ", """please input either "y" or "n".""", ["y", "Y", "n", "N"])
        if yn in ["y", "Y"]:
            j=0
            score=0
            while j < len(triv):
                x=typeIn("Please enter the name of the capitol of " + triv[j] + " or q to quit.")
                if x == states[triv[j]]:
                    print("\n\nCorrect!!!!")
                    score += 1
                if x not in [states[triv[j]], "q"]:
                    print("That's totally wrong!!!!")
                    print("It was actually ", states[triv[j]], ".", sep="")
                if x == "q":
                    yn = "n"
                    break
                if j == len(triv): break
                j += 1
        if yn in["n", "N"]:
            print("Your score is", score)
            break
                    # Now! This is where the real fun/terrifyng code
                    # starts I think its all working now there was a time
                    # when it seemed a little messed up, and well see if
                    # it all works the same on your wendoze machine. This
                    # is a program that uses the Vigenere Cipher to
                    # encrypt a file. I thought this was wuch more fun
                    # than just a simple replacement cipher and its also
                    # much harder to break I think it also has possible
                    # key lengths equal to the length of the file being
                    # encrypted so theres that. so this first function
                    # just strips away carachters we're not going to use,
                    # I'm guessing that any file were going to try will
                    # be ascii stuff i just gor rid of \xob/c whatever
                    # they are (I think they are for moving around the
                    # screen.
def stripString(string):
    return [i for i in string if i in printable[0:97]]
                    # Dictionary! this make the Vigenere table i might
                    # suggest running the assignment from thp python repl
                    # with exec(open("HW9.py").read(), globals()) that
                    # way you con play around with the functions
                    # afterwards this one looks pretty cool.
def vigTable(): 
    return dict(zip(list(printable[0:97]), [printable[i:97] + printable[0:i] for i in range(len(printable[0:97]))])) 
                    # This one is for the Vigenere key sequence which
                    # takes any text (and since my vigenere table
                    # includes spaces and new lines and all that it can
                    # really be as long as you want. this function
                    # extends it so that the number of carachters match
                    # the number in the file.
def keyString(key, string):
    x=len(string)//len(key) 
    y=len(string)%len(key) 
    if len(key) < len(string): 
        return list(key * x + key[0:y])
    if len(key) >= len(string): 
        return list(key[0:len(string)])
                    # heres the magic basically looks up the
                    # corresponding rotation based on key character.
def vigEncipher(keystrlist, stringlist):
    return [printable[0:97][vigTable()[i].index(j)] for i,j in zip(keystrlist, stringlist)]
                    # basically the opposite of above
def vigDecipher(keystrlist, stringlist):
    return [vigTable()[i][printable[0:97].index(j)] for i,j in zip(keystrlist, stringlist)]
                    # main function, this is a hot mess! So I reused the
                    # file opening dialogue from the last assignment.
                    # Which I dont really like I know how to fix it but
                    # these assignments keep on comin so it is what it
                    # is. It helps you find a file then input a key
                    # phrase. then opens the file and creates the full
                    # length key then encryps the file and writes in with
                    # a "EN_" prefix. and writes the full length key to a
                    # file with a "KEY_" prefix. if you choose to decrypt
                    # the file. it helps you find an appropriate file
                    # with a "EN_" prefix, then checks for tho existance
                    # of a "KEY_" prefix file of the same name. If all
                    # that is present and accounted for it should decrypt
                    # the file. I would say opening the file in windows
                    # might be the downfall of this whole mess, but I
                    # don't have a windows computer handy and times a
                    # waistin. So I hope it works, but I think I,m gonna
                    # probably improve this and rewrite it as an
                    # executable script in julia for personal encryption,
                    # cause thats ZuP3r L337!
def ex_3():
    print("Let's do some hard cryption!!")
    print("\n\n", "This program will write an encrypted version of a text file of your choosing, and a key file containing the required key for decryption.")
    en_de = input("""Are you encypting or decrypting? Type en to encrypt a file or de to decrypt. Keep in mind you need to have a KEY file created by this program in the same directory to decrypt. The encrypted file will start with "EN_".\n[en de]>  """)
    sleep(1)
    print("\n\n")
    cwd = os.getcwd()
    _yn = "c"
    while _yn in ["c", "C"]:
        for i in os.scandir(os.getcwd()):
            if i.is_file():
                print(i.name)
        print("These are the files in your current directory.")
        sleep(1)
        print("\n\n")
        for i in os.scandir(os.getcwd()):
            if i.is_dir():
                print(i.name)
        print("These are sub directories in your current directory.")

        sleep(1)
        print("\n\n")
        _yn = strLoop("Would you like to read a file in the current directory, or navigate to a new directory to find a file? Please enter y to open a file here or c to choose a new directory.\n[y,c]>  ", "Either y or c please.", ["y", "n", "c", "C"])
        if _yn in ["c", "C"]:
            while True:
                try:
                    os.chdir(input("OK. Please enter the name of the directory.\n[dir name]>  "))
                    break
                except FileNotFoundError:
                    print("It needs to be a directory in the current directory or a fully qualified pathname.")
    if _yn in ["y", "Y"]:
        while True:
            fncheck = "         "
            if en_de == "de":
                while fncheck[0:3] !="EN_":
                    fn = input("Please enter the name of the encrypted file starting with EN_.\n[EN_filename]>  ")
                    if fn[0:3] == "EN_":
                        if "KEY_"+fn[3:] in [i.name for i in os.scandir(os.getcwd()) if i.is_file()]:
                            fncheck = fn
                        else:
                            print("\nThere is no suitable keyfile in this directory.")
                    else:
                        print("Seriously?")

            if en_de == "en":
                    fn = strLoop("\nPlease enter the name of the file to encrypt.\n[filename]>  ", "That file is not in this directory.", [i.name for i in os.scandir(os.getcwd()) if i.is_file()]) 
            try:
                with open(fn) as faiul:
                    text = faiul.read()
                    while True:
                        if en_de == "en":
                            keyput = input("Please enter a key with ascii carachters only\n[ascii]>  ")
                            for i in keyput:
                                if i not in printable:
                                    print("ascii please")
                                    continue
                        break
                    break
            except FileNotFoundError:
                print("It needs to be a directory in the current directory or a fully qualified pathname.")
        if en_de == "en":
            stringlist = stripString(text)
            key = keyString(keyput, text)
            en_text = vigEncipher(key, stringlist)
            with open("EN_" + fn, "w") as EN_file:
                EN_file.write("".join(en_text))
            with open("KEY_" + fn, "w") as KEY_file:
                KEY_file.write("".join(key))
            print("Your file has been encrypted with the Vigenere Cipher! and saved to EN_" + fn, "\nThe key file has been saved to KEY_" + fn)
        if en_de == "de":
            with open("KEY_"+fn[3:]) as key_file:
                key = list(key_file.read())
            stringlist = list(text)
            de_text = vigDecipher(key, stringlist)
            with open("DE_"+fn[3:], "w") as DE_file:
                DE_file.write("".join(de_text))
            print("Your file has been decrypted (I hope). It can be found in this directory saved as DE_"+fn[3:]+".")




                    # Copy paste from other assignment
prawnz = ["\nPlease select which programming exercise you would like to run, or q to exit:\n[1,2,3 or q]>  "]  

                    # This is the main loop. It asks for input of the
                    # exercise number and then runs the corresponding
                    # function. It will continue looping until the letter
                    # "q" is entered.

def mainList():
    ex_num = 'foo' 
    while ex_num != 'q': 
        print("\n")
        ex_num = typeIn(prawnz[0])
        if ex_num == 1:
            ex_1()
        elif ex_num == 2:
            ex_2()
        elif ex_num == 3:
            ex_3()

                    # run that puppy!
mainList()

































                    # such a empty! No jokes for you!
