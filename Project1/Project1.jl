#=  Caspian Baskin
    EGR 115
    Project #1
    Feb,27,2019

        This is the first project for my programming for engineers class. 
    The program calculates the volume and mass of three selected 3d shapes.
    I have chosen a helix (in the guise of a spring with all active coils),
    a composite shape consisting of half of a torus surface with a uncappd cylinder bonded to the hole (in the guise of a spring lock such as those found on automotive valve springs, in reality the cylinder would be a sliced hollow cone),
    and a contrived spring seat consisting of a cylinder with half a torus removed from the top (Be aware that this setup in real life would need a "closed" coil spring with some sort of end treatment for this seat to be functional but Cas gets what Cas wants and the functionality of the shapes has little bearing on the assignment).
    I have chosen to write this program in julia language rather than python because I beleive that in the coming years it will be an important part of most fields requiring numerical and symbolic analysis, but due to it's young age and only recently reaching a stable release, there are no classes offering instruction. Python itself has become an important tool for this type of work but due to its design can often require code in other languages to supplement it for speed in working with numbers and symbolic calculations.
    Julia hopes to solve those problems with a JIT and dynamic compillation among other things.

=#

function input(prompt::String=""):Any #julia does not have an input function built in like python, it is reccomended that user inputs should be arguments to a function(becaus this is a python class and the assignment is designed for that I define this one). this requires that the prompt is a string but the output type can be anything, I think this is a pretty usefull feature.
    print(prompt) #prompt for input
    word = chomp(readline()) #read from stin remove \n and store
    inum = tryparse(Int64, word) #try parse will make into an integer if its a number with no decimals otherwise it will assign "nothing::Void" equivalent to python's None.
    dnum = tryparse(Float64, word) #the same for floating point numbers
    if typeof(inum) == Int64 #check type and output converted value
        inum
    elseif typeof(dnum) == Float64
        dnum
    else
        word
    end
end

#= It seems no be reccomended to put as much of your program as possible into functions. 
    Supposedly this speeds up due to the JIT.
    I have tried to do this here, who knows if I'm doing it right
=#

function volumeLock(r,R,thick) #calculate volume for spring lock
    ((2*π*r)*(2*π*R)*(0.5)+(2*π*(R-r)*(r*2)))thick #flatten half a torus and lateral surphace of cyllinder then multiply by proposed plate thickness.
end


function volumeSeat(r,R,thick) #volume for spring seat
    π*(r+R)^2 * (r+thick) - (π*r^2)*(2*π*R) #volume of cylinder with outer diameter equal to the spring and then subtract volume of torus section.
end


function volumeSpring(r,R,height,n) #volume for spring
    2*π^2*n*R*r^2 #This formula came from wikipedia and some spring sites, I have breifly tried to verify it but not extensively. fortunately this is a programming assignment.
end


function maSS(V,D) #mass
    V*D #massive calculation
end


function springConst(d,D,n) #spring constant calc I was going to try and have the user input the forces the spring would be subjected to in order to calculate the pitch and wire length, but I decided to simplify my life instead.
    (material[G]*d^4)/(8*D^3*n) #G = shear modulous this calculates the spring rate
end


function maxForce(k,height,heightMin) #calculate the force stored in the spring at its solid height for no reason
     k*(height - heightMin)
end


function coilsNum(d,heightMin) #determine the most number of coils the spring can have based on specified minimum height
    heightMin/d
end


function switchReset() #julia has pretty strict variable scope compared to python which makes using a switch to control a loop trickier so once i figured it out i put it in a function
    global yn #make this variable global for @#&$'s sake
    yn = "y" #reset switch the next function does the same thing but it's more readable this way
end


function switchY() #^ see above
    global yn
    yn = "y"
end


function switchN() #trip the switch to get out of a loop
    global yn
    yn = "n"
end

 #This is where it finally dawned on me that its a function not code storage, it doesnt change variables it outputs a value for a given input, duh. not sure why this "revalation" took so long, geeze.
 function loopq(x) #extension of input() that will only return a Number or an error and restart, used to ensure the user is not just typing "boob" and giggling over and over (which is how it's tested incidentally)
    a = "a" #local variable::String
    while typeof(a) <: AbstractString #this was tough because my input function returns a SubString type while explicit declaration returns a String type it took forever to figure out I wanted AbstractString which is the supertype for things stringy
        a = input(x) #call input function
        if typeof(a) <: AbstractString #if input is a string of any type
            print("""\nPlease enter a "number" in $(unit).\n\n""") #insult
        elseif typeof(a) <: Number #if it's a number
            return a #say it again
        end
    end
end

musicW = Dict(:Gpa => 7.9e10, :Gpsi => 1.2e7, :Densitygmm3 => .007860, :Densitylbin3 => 0.284, :MaxDin => 0.250, :MaxDmm => 6.350, :MinDin => 0.004, :MinDmm => 0.1) #music wire specs

berylC  = Dict(:Gpa => 4.83e4, :Gpsi => 7e6, :Densitygmm3 => .008260, :Densitylbin3 => 0.298, :MaxDin => 0.375, :MaxDmm => 9.50, :MinDin => 0.003, :MinDmm => 0.08) #beryllium copper specs

stain302 = Dict(:Gpa => 6.9e4, :Gpsi => 1e7, :Densitygmm3 => .007920, :Densitylbin3 => 0.286, :MaxDin => 0.375, :MaxDmm => 9.50, :MinDin => 0.005, :MinDmm => 0.130) #stainless 302 specs

switchReset() #"y"

while yn == "y" #start overall loop so we can do mustiple objects

    switchReset() 

    while yn == "y" #start another error checking loop so if you mess up only one you dont have to start all over
        object = input("\nPlease choose which part you are making to calculate its mass and weight at standard gravity.\n\n    S\t\t\t    E\t\t\t    L\nFor Spring\t\tfor spring sEat\t\tfor spring Lock plate\n\n>  ") #ask for an easy to type letter

        global yn #declare my variable for all the world to see
        global object
        if object == "S" #if loop that changes the letter to a word for better code readability
            object = "spring"
            switchN()
        elseif object == "E"
            object = "seat"
            switchN()
        elseif object == "L"
            object = "lock"
            switchN()
        else
            print("\nPlease choose from the list.") #insult
            switchY()
        end
    end

    switchReset()

    while yn == "y" #second preliminary question
        material = input("\nNow specify which material is being used.\n\n    M\t\t\t\t\t    B\t\t\t\t\t    S\nFor spring steel(music wire)\t\tFor beryllium copper\t\t\tFor stainless302\n\n>  ") #ask for material
        global material 
        global yn
        if material == "M" #now changevariable to spec list name
            material = musicW
            switchN()
        elseif material == "B"
            material = berylC
            switchN()
        elseif material == "S"
            material = stain302
            switchN()
        else
            print("\nPlease choose one from the list.\n") #insult
            switchY()
        end
    end



    switchReset()

    while yn =="y" #last preliminary question
        unit = input("""\nEnter units of "mm" or "in"\n>  """) #find out if we're US or somewhere terrible::Joke
        global unit #declare lots of global variables I have since learned that "global x = 2" will suffice but this conveys the message a little better so I left it for now
        global min
        global max
        global force
        global G
        global kC
        global dens
        global maz
        global yn
        if unit == "mm" #assign variables to list index symbols and unit names for question asking
            min = :MinDmm
            max = :MaxDmm
            G = :Gpa
            dens = :Densitygmm3
            maz = "grams"
            force = "Newtons"
            kC = "newtons per meter"
            switchN()
        elseif unit == "in"
            global unit
            unit = "inches"
            min = :MinDin
            max = :MaxDin
            G = :Gpsi
            dens = :Densitylbin3
            maz = "lbm"
            force = "lbf"
            kC = "pounds per inch"
            switchN()
        else
            print("\nPlease choose from the list.") #insult
            switchY()
        end
    end


    D=0.0 #declare some variables to make the errors go away see below ||
    d=0.0 #                                                            \/
    height=0.0
    Fmax=0.0


#= So another thing I learned,
            In python print("string", var, "string) is how you do in string interpolation,
            in julia print("string $(var) string") it's inline.
            So when you declare a list(Dict) it interpolates it and saves it to memory so if you have undeclared variables it's error city (not a great place).
            So rather than try to solve the problem logically, i just made dummy variables and put the list into a function so I can call it over and over! lazy? yes please!
=#


    function query()
        global objectQuery
        objectQuery = Dict(:SpringD => "\nPlease enter the median diameter of the spring in $(unit).\nSpring diameter - wire diameter.\n\n>  ", :Springd => "\nPlease enter a wire radius for the spring between $(material[min])$(unit) and $(material[max])$(unit).\n\n>  ", :SpringLmax => "\nEnter the maximum height. Should be larger than $(d*5)$(unit).\n\n>  ", :SpringLmin => "\nEnter the minimum compressed height in $(unit) larger than $(d*4)$(unit) and smaller than $(height)$(unit).\n\n>  ", :SpringFmax => "\nThe maximum applied force for spring is $(Fmax) $(force).\n\n>  ", :SeatD => "\nPlease enter the median diameter of the spring that will go in the spring seat.\nI.E. across the spring measured from the center of the wire.\n\n>  ", :SeatThick => "\nEnter a thickness for the thinnest portion of the seat in $(unit).\n\n>  ", :LockD => "\nPlease enter the desired diameter of the spring helix in $(unit).\n\n>  ", :LockThick => "\nEnter the desired thickness for the spring lock in $(unit), keep in mind it needs to be less than $(D-2*d-.001)$(unit).\n\n>  ")
    end

    query() #load list

    if object == "lock" #determine which shape and ask appropriate questions

        D = loopq(objectQuery[:LockD]) #get diameter of spring::Number
        R = D/2 #radius of spring

        d = loopq(objectQuery[:Springd]) #diameter of wire::Number
        r = d/2

        query() #load list again

        thick = loopq(objectQuery[:LockThick]) #thickness

        volume = volumeLock(r,R,thick) #calculate volume

        mass = maSS(volume,material[dens]) #calculate mass

        print("\n\n\nThe upper spring lock for your $(D)$(unit) diameter spring made from $(d)$(unit) diameter wire.\n\n") #define spring lock for the user

        print("The volume of the spring lock is $(round(volume; digits=3)) $(unit)^3\n\n") #tell volume

        print("The mass of the spring lock is $(round(mass; digits=3)) $(maz) \n\n") #tell mass
    elseif object == "seat"

        D = loopq(objectQuery[:SeatD]) #same as above
        R = D/2

        d = loopq(objectQuery[:Springd]) #ditto
        r = d/2

        thick = loopq(objectQuery[:SeatThick]) #thickness of thinnest part of seat so that the cylinders height is this + height of torus

        volume = volumeSeat(r,R,thick) #calc volume

        mass = maSS(volume, material[dens]) #calc mass

        print("\n\n\nThe spring seat for your $(D)$(unit) diameter spring made from $(d)$(unit) diameter wire.\n\n") #define spring seat

        print("The volume of the spring seat is $(round(volume; digits=3)) $(unit)^3\n\n") #tell volume

        print("The mass of the spring seat is $(round(mass; digits=3)) $(maz) \n\n") #tell mass


    elseif object == "spring" 

        D = loopq(objectQuery[:SpringD]) #uh huh
        R = D/2

        d = loopq(objectQuery[:Springd]) #yep
        r = d/2

        query() #load list again

        height = loopq(objectQuery[:SpringLmax]) #ask for unloaded spring height
        
        query() #load list again again

        heightMin = loopq(objectQuery[:SpringLmin]) #get a specified loaded height

        n = coilsNum(d,heightMin) #calculate maximum coils based on height requirements (not very realistic)

        k = springConst(d,D,n) #calc spring rate for no reason

        Fmax = maxForce(k,height,heightMin) #calc stored force at minimum height for no reason

        volume = volumeSpring(r,R,height,n) #calc volume

        mass = maSS(volume, material[dens]) #calc mass

        print("\n\n\nYour spring will be $(D) $(unit) diameter from $(d) diameter wire. \n\nIt has $(n) turns. \n\nThe spring constant is $(k) $(kC), \n\nand the maximum force at minimum height is $(round(Fmax; digits=3)) $(force).\n\n") #define spring and hanh out some useless information

        print("The volume of the spring is $(round(volume; digits=3)) $(unit)^3 \n\n") #tell volume

        print("The mass of the spring is $(round(mass; digits=3)) $(maz) \n\n") #tell mass

    end
    global yn
    yn = input("\n\n\nWould you like to calculate another part?\n\ny/n>  ") #ask to start loop over

end
