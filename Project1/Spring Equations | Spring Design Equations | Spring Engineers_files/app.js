/*
 Copyright (C) 2007 Free Software Foundation, Inc. http://fsf.org/
 */

/*
 Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
 */
;
(function ($, window, undefined) {
    'use strict';

    var $doc = $(document),
        Modernizr = window.Modernizr;

    window.SpringHouston = {};

    SpringHouston.switchClasses = function ($content) {
        if ($content.hasClass("short-text")) {
            $content.removeClass("short-text").show("slow");
            $(this & ">.arrow").removeClass("arrow-down").addClass("arrow-up");
        } else {
            $content.addClass("short-text");
            $(this & ">.arrow").removeClass("arrow-up").addClass("arrow-down");
        }
    };

    SpringHouston.getShowLinkText = function (currentText) {
        var newText = '';

        if (currentText === "READ MORE") {
            newText = '<i class="icon-angle-up"></i>&nbsp;READ LESS';
        } else {
            newText = '<i class="icon-angle-down"></i>&nbsp;READ MORE';
        }

        return newText;
    };

    SpringHouston.initiateSlider = function (selector) {
        $(selector).orbit({
            timer_speed: 5000, // Sets the amount of time in milliseconds before transitioning a slide
            pauseOnHover: true,
            startClockOnMouseOut: true,
            startClockOnMouseOutAfter: 1000,
            bullets: true,
            bulletThumbs: true,
            bulletThumbLocation: "/images/bulletThumbs/"
        });
    };

    SpringHouston.toggleReadMoreBlock = function () {
        $(".show-more").on("click", function () {
            var $link = $(this);
            var $content = $(".copy-wrapper");

            var linkText = $link.text().toUpperCase().trim();

            SpringHouston.switchClasses($content);

            $link.html(SpringHouston.getShowLinkText(linkText));

            return false;
        });

        if ($(".copy-wrapper").height() < 235) {
            $(".show-more-wrapper").hide();
        }
    };

    SpringHouston.setActiveClasses = function () {
        $(".tabs dd:first-child")
            .add($('.tabs-content, .accordion').find('li:first-child'))
            .addClass('active');

        $(".accordion.materials li:first-child")
            .removeClass('active');
    };

    SpringHouston.setupCtas = function (buttonSelector, targetSelector) {
        $(buttonSelector).click(function () {
            event.preventDefault();

            $('html, body').animate({
                scrollTop: $(targetSelector).offset().top
            }, 1000);

            return false;
        });
    };

    $(document).ready(function () {
        $.fn.foundationAlerts ? $doc.foundationAlerts() : null;
        $.fn.foundationButtons ? $doc.foundationButtons() : null;
        $.fn.foundationAccordion ? $doc.foundationAccordion() : null;
        $.fn.foundationNavigation ? $doc.foundationNavigation() : null;
        $.fn.foundationTopBar ? $doc.foundationTopBar() : null;
        $.fn.foundationCustomForms ? $doc.foundationCustomForms() : null;
        $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
        $.fn.foundationTabs ? $doc.foundationTabs({callback: $.foundation.customForms.appendCustomMarkup}) : null;
        $.fn.foundationTooltips ? $doc.foundationTooltips() : null;
        $.fn.foundationMagellan ? $doc.foundationMagellan() : null;
        $.fn.foundationClearing ? $doc.foundationClearing() : null;

        $.fn.placeholder ? $('input, textarea').placeholder() : null;

        SpringHouston.initiateSlider("#slider");

        // set search placeholder
        $("#search").val("");

        SpringHouston.setupCtas('.cta-quote.full a.button', '.contact-form-inline.full');
        SpringHouston.setupCtas('.cta-quote.mobile a.button', '.contact-form-inline.mobile');

        // Toggle Read More Block
        SpringHouston.toggleReadMoreBlock();

        SpringHouston.setActiveClasses();

    }); // end document.ready

    // UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE8 SUPPORT AND ARE USING .block-grids
    $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
    $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
    $('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'both'});
    $('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'both'});

    // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
    if (Modernizr.touch && !window.location.hash) {
        $(window).load(function () {
            setTimeout(function () {
                window.scrollTo(0, 1);
            }, 0);
        });
    }

    // adding necessary classes for Form Stack submit buttons to be styled
    $('input.fsSubmitButton').removeClass('fsSubmitButton').addClass('radius button medium gradient red');
})(jQuery, this);

// Responsive Menu
$(function () {
    $('#dl-menu').dlmenu();
});
