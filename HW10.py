#!/usr/bin/env python3
                    # This first line can be removed if it doesnt work on
                    # Windows, but it should work fine.
#Caspian Baskin
#EGR 115
#Csv Parser
# @2019

                    # I realise that a lot of this is uneccessary but I
                    # want to take advantage of the fact that i have
                    # someone who knows more abouh python and prgramming
                    # than I do, that has to help me if I cant figure
                    # something out. So I'm trying all kinds of stuff.
                    # For this one I used named tuples for the blueprint
                    # specs, and argparse so it can be run from the
                    # command line. I'm sure that's not exactly what you
                    # wanted but there it is.
from collections import namedtuple
import argparse
import os
                    # So I set this one up (since it is only one
                    # exercise) to be run from the command line. It
                    # should work on any unix or mac shell perfect. It 
                    # will work in power shell I tested it use the same 
                    # syntax below, and you can use tab completion. If it
                    # doesn't you'll have to comment out the argparse
                    # stuff and manually change the filenames of the two
                    # open statements.

                    # To run it just navigate to the folder containing
                    # this file and, on unix or mac (I think) type
                    # "./HW10.py part_data.csv" and it should work. you
                    # can also change the output filename like 
                    # "./HW10.py -f awesome_output_file part_data.csv. On
                    # windows it should work without the initial "./".
                    # you could also call other csv files to parse but it
                    # would have to be for the same part. and finally you
                    # can get a description of all the options with
                    # "./HW10.py --help".
#parser = argparse.ArgumentParser("""This program will check the "High-Strength Steel Cap Screw Grade 8 #92620A401"  measurement data.csv file against the blue print specs and write out a pass fail csv file.""")
#parser.add_argument("-f", type=str, help="The name of the csv file you want to check. Can include a path if not in current directory.")
#parser.add_argument("-o", "--out", default="party_part_part.csv", help="Option to name the #output file. defaults to party_part_part.csv")
#args = parser.parse_args()


def typeIn(promt):
    stuff = input(promt)
    try:
        stuff = float(stuff)
        if stuff%1 == 0:
            stuff = int(stuff)
        return stuff
    except ValueError:
        return stuff

def strLoop(_prom, _insult, _list):
    while True:
        x = typeIn(_prom)
        if type(x) == str and x in _list:
            return x
            break
        else:
            print(_insult)


snore = strLoop("""\nThis program will check the "High-Strength Steel Cap Screw Grade 8 #92620A401"  measurement data.csv file against the blue print specs and write out a pass fail csv file.\nPlease enter the name of the file\n>  """, "\nUnfortunately it needs to be a file in the current working directory.\n", [x.name for x in os.scandir(os.getcwd()) if x.is_file()])

dull = input("""Enter the name of the results file, if you would like. Otherwise it will default to "party_part_resulties_stuff_and_junk.csv".\n>  """)

if dull == "":
    dull = "party_part_resulties_stuff_and_junk.csv"
                    # My favorite function
def drange(high, low=1, step=1):
    _low = low
    while True:
        if _low > high:
            return
        yield _low
        _low += step
                    # This block names and stores the blueprint specs in
                    # a named tuple allawing them to be called like
                    # head.tol unneccesary but fun! Oh also I'm really
                    # trying to figure out how to alter python itself
                    # (mostly just so I can make it do what I want and
                    # not what is best). to that end I have been
                    # experimenting with exec eval and the ast module.
                    # Theres none of that here, but I've read many things
                    # stating that eval is dangerous so I used it as much
                    # as i possibly could. Although I dont think theres
                    # any danger here due to the fact that it only
                    # operates on internally defined strings not user
                    # input.
Specs = namedtuple("Specs",["des","tol"])
head = Specs("3/16",".010")
length = Specs("1/4", ".005")
diam = Specs(".112", ".003")
                    # Ahhh this is nice, probably wrong and definetly not
                    # pythonic but something I've been trying to do the
                    # whole time generate variable names programatically!
                    # I'll make this python a lisp yet!
for i in ["head", "length", "diam"]:
    x=eval(i)
    exec(i + "Range = list(drange(round(eval(x.des),3)+eval(x.tol), round(eval(x.des),3)-eval(x.tol), .001))")
                    # read in the file as a list of lines. Then steal the
                    # first one with all the labels. then steal the part
                    # number which is the same for all of them' then zip
                    # up whats left into a dictionary with serial numbers
                    # as keys and measurements as values in a list.
with open(snore) as csv:
    meas=csv.readlines()
label = meas[0]
del meas[0]
pn = meas[0].split(",")[0]
meas = dict((i.split(",")[1], [j.strip() for j in i.split(",")[3:]]) for i in meas)
                    # Make a whole bunch of lists! But seriously list
                    # comprehensions that check the measurements against
                    # the blueprint range lists and then inserts PASS or
                    # FAIL into the new list. Then do the same thing but
                    # with individual parts PASS or FAIL values.
dList = ["PASS" if eval(meas[x][0]) in diamRange else "FAIL" for x in meas]
lList = ["PASS" if eval(meas[x][1]) in lengthRange else "FAIL" for x in meas]
hList = ["PASS" if eval(meas[x][2]) in headRange else "FAIL" for x in meas] 
resultList=["PASS" if "FAIL" not in x else "FAIL" for x in zip(dList, lList, hList)]
                    # Frankenstein comprehension! this combines all our
                    # generatetd lists with the part numbers, and
                    # a comma and spaces in between each item for easier
                    # perusal.
List=[(","+" "*9).join([pn,x,y,z,i,j,]) for x,y,z,i,j in zip(meas.keys(), dList, lList, hList, resultList)]
                    # give back the labels
List.insert(0, label)
                    # write the resulting list to the file specified by
                    # the user, or the default filename.
with open(dull, "w") as testCsv:
    for i in List:
        testCsv.write(i + "\n")
                    # DUN!
