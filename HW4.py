#Caspian Baskin
#EGR 115
#HW4
#Feb,28,2019

#goodies
from time import sleep
from random import randint
#common response lists (boolean)
yList = ["y","Y","yes","Yes","Yes!", "YES", "YESSIR!"]
nList = ["n","N","no","No","NO","NO!","No!", "NOWAY!"]
#a list of insults for the final user. Cause it's fun?
insults = ["Do you even want dinner?", "Oh you're some kind of comedian huh?", "Just yes or no typed in some kind of reasonable way please.", "Did Scott put you up to this?", "I swear,...    I just...            forget it", "C'mon were all hungry here and you're just playing games?", "Just type yes or y or no or even NO! if you fancy it, I mean it's not that hard."] 


#setup for while loops
yn = "No"

#main list prompt
prompt = "Please select which programming exercise you would like to run, 3,17,18 or q to exit: " 
ex_num = 'foo'  #Initialize list input variable to something other than the value in while statement
while ex_num != 'q': #start main list loop
    print("\n")                     
    ex_num = input(prompt)  #ask for main list input
#Program that makes ageist remarks based on user input
    if ex_num == '3':
        prompt_age = "\nPlease enter your age.\n>  " #get age
        try: #no errors please
            age = int(input(prompt_age)) #really get age
            if age == 1: #find out if they're 1
                print("You're just a baby, baby.") #how are they using a computer?
            elif age in range(1,14): #find out if they're kids
                print("You're just a child, child.") #tell them so
            elif age in range(14,20): #find out if they're teens
                print("You're a teenager!") #tell them
            elif age in range(20,80): #find out if they're old
                print("You're an adult!") #tell
            elif age >= 80: #really old
                print("Wow you are super old!!!") #tell
            else:
                print("I don't even know what that is...") #decide it's not working out
        except ValueError:
            print("Ooh, you're not even old enough to type :( ") #insult
#Program to essentially tell people to turn everything off then back on again when theres a wifi problem
    elif ex_num == '17':
        yn = "NOWAY!"
        def yenae(): #function so i dont have to re type it everytime i mean, function for people who aren't really sure how to answer
            sleep(1)
            global yn 
            yn = input("""\n\nDid it work?\n["y" or "n"]  """) #find out if we're actually gonna have to fix something
        
        while yn in nList: #is it still broken?
            print("\nWe are going to try and get you're wifi working,\n") #comfort
            sleep(1)
            print("Now reboot the computer, and then try again to connect") #a statement it has never made (unless its a cell phone) it's turn it off unplug remove battery... undo
            yenae()             
            if yn in nList: 
                print("\nOk. Now, reboot the router and try to connect,\n") #from here on out check if it's fixed and if not plod on with all the questions
                yenae()
                if yn in nList:
                    print("\nAlright,now make sure the cables between the router and modem are plugged in properly, \n")
                    yenae()
                    if yn in nList:
                        print("\nMove the router to a new location and try to reconnect,\n")
                        yenae()
                        if yn in nList:
                            print("\nRouter's busted, get a new one\n.")
                            yn = "yes"
            elif yn in yList: #find out if we're off the hook
                print("\nWould you look at that, tech support triumphs again\n") #pat ourselves on the back
                yn = "Y"
            else: #if they absolutely can't work it
                sleep(2)
                print("\n", insults[randint(0,6)], "\n")
                yn = "NO!" #cry about it
#program to suggest restaurants based on allergies and preferences
    elif ex_num == '18':
        wasteOFtime = [] #bad response list
        rest = {"joe":"Joe's Gourmet Burgers", "pizza":"Main St Pizza", "cafe":"Corner Cafe", "mama":"Mama's Fine Italian", "chef":"The Chef's Kitchen"} #names and identifiers for restaurants in a dict (don't say it fast)
        notVeg = ["joe"] #not vegitarian
        notVee = ["joe", "pizza", "mama"] #not vegan
        notGlu = ["joe", "mama"] #not gluten free

        def alergz(promp,lisT): #get a response and adjust potential restaurant list or come to the realization that the user is not taking this seriously
            allergik = input(promp) #ask question
            if allergik in yList: #affirmative?
                for i in lisT: #if so
                    x = i #new variable just in case (I'm not really sure what happens to the i
                    global rest #make sure we can play nice with restaurant list
                    if x in rest.keys(): #see if any bad candidates are in the list (using dict keys)
                        del rest[x] #you're OUTTA HERE!!!
            global wasteOFtime #play nice with bad response list
            if allergik not in yList and allergik not in nList: #so if input is meaningfull
                sleep(.3) #dramatic pause and...
                print("\n", insults[randint(0,6)],"\n") #INSULT!!! man CS can be fun
                sleep(1.4) #let it sink in
                wasteOFtime.insert(1,1) #naughty
        def goodRest(): #another function so i can type less
            for i in rest.values(): #get remaining restaurants from modified list
                print(i, "\n") #output
                sleep(.4) #looks better

        alergz("\nIs anyone in your party vegitarian?i\n\n[yes or no]  ",notVeg) #ask and do the stuff
        alergz("\nIs anyone in your party vegan?\n\n[yes or no]  ",notVee)
        alergz("\nIs anyone in your party gluten free?\n\n[yes or no]  ",notGlu)
        if len(wasteOFtime) == 0: #see how many responses are good and provide increasingly harsh responses
            print("\nYou can take your peoples to,\n")
            goodRest()
        if len(wasteOFtime) == 1:
            print("\nWell maybe you can take your friends to,\n")
            goodRest()
        if len(wasteOFtime) == 2:
            print("\nWell maybe you just don't actually want to go out to eat. But you could try,\n")
            goodRest()
            print("I hope you're friends don't get sick.")
        if len(wasteOFtime) == 3: #if they're all garbage then just forget about the whole thing
            print("Forget it you probably should'nt be going anywhere anyways.")
            sleep(1.4)
