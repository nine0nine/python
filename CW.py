#Caspian Baskin
#EGR 115
#
import math
height = input('What is the height of the rectangle? ')
width = input('What is the width of your rectangle? ')

try:
    fheight = float(height)
except ValueError:
    print('You are a dummy!')

try:
    fwidth = float(width)
    area = fheight * fwidth
    perim = fheight * 2 + fwidth * 2
    print('\n')
    print('The area of the rectangle is:', area)
    print('\n')
    print('The perimeter of the rectangle is:', perim)
    print('\n')
    print('And pi is...')
    print(math.pi)
except ValueError:
    print('In numbers please.')



