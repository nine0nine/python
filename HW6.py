#Caspian Baskin
#EGR 115
#HW6
#Some time in March

                    # This chapter was about functions, so the homework 
                    # is functions. It was tricky, but I've 
                    # spent a little time on it and it seems a little 
                    # clearer tham it did at first. It is definetly 
                    # harder to debug this way, but maybe thats just 
                    # python. I've been trying to learn about object 
                    # oriented programming, and I don't really like what 
                    # I see. but heres my best try at functional 
                    # programming, while trying to keep it somewhat sane.
                    # 
from time import sleep
import random

                    # This is a function that loops an input question 
                    # until an appropriate number is entered
def numLoop(prom, insult):
    while True:
        x = typeIn(prom)
        if type(x) == int or type(x) == float:
            return x
            break
        else:
            print(insult)
                    # The same as the previous function but for a string
                    # I don't think I actually used this one at all
def strLoop(prom, insult):
    while True:
        if type(x) == str:
            return x
            break
        else:
            print(insult)
                    # This is a redefined input function that types the
                    # input string
def typeIn(promt):
    stuff = input(promt)
    try:
        stuff = float(stuff)
        if stuff%1 == 0:
            stuff = int(stuff)
        return stuff
    except ValueError:
        return stuff
                    # a function to clear the output screen
def clear_screen():
    print("\n" * 150)

                        #Functions for exercise 1
                    # Convert kilometers to miles
def milesAreBetterThanKilometers(kill):
    return kill *.6214
    
                    # a function to format as a string and add units to
                    # a numeric input in miles 
def ex1_data(mNum):
    return format(mNum, ',.2f') + " miles"
                    # This Function is the main body of the exercise and
                    # so does a few things. First it clears other output,
                    # then explains the program, Then it uses the prompt 
                    # list to ask for input in kilometers. Finally it 
                    # outputs the properly formatted result.
def ex_1():
    clear_screen()
    print(prawnz[1])
    sleep(1)
    a = numLoop(prawnz[2], insult[0])
    sleep(1)
    print("\n", ex1_data(milesAreBetterThanKilometers(a)), ", is equivalent to, ", a, "kilometers.")
                #Functions for exercise 7

                # This function returns the sum of elements lists 
                # contained in dictionary dict2, which should be a
                # dict with lists for items.
def listSum():
    return [sum(dict2[i]) for i in dict2.keys()]
                # This function asks the user to input their sales
                # for each class of tickets, and allows them to do this
                # more than once by appending sales numbers to a list
                # item in a dictionary.
def seatSales(Class):
    a = numLoop(prawnz[4] + Class + prawnz[5], insult[0])
    x = dict2[Class]
    x.append(a)
    dict2[Class] = x
                # A function that creates two dictionaries, one with the
                # prices for each class of ticket with the class letter
                # as keys, anh the other with empty lists, and the class 
                # letters as keys.
def dictGen():
    global dict1
    global dict2
    dict1 = {x:y for x,y in zip(["A", "B", "C"], list(range(20,5,-5)))}
    dict2 = {x:[] for x in dict1.keys()}
                # The wain function for example 7. Clear the screen 
                # (which doesn't seem to work in idle with automatic
                # line folding), generate dictionaries, and explain
                # the program. Then ask for sales input and finally 
                # ask if the user has more sales to input. Then sum
                # each list for each class (in case of multiple inputs)
                # in a new list. Then create a list of profits for each
                # class by multiplying the sum list with the prices
                # elementwise. Then output total ticket sales and the 
                # total money raised for each class. Then output the 
                # total of tickets sold and the total money raised.
def ex_7():
    clear_screen()
    dictGen()
    print(prawnz[3])
    sleep(8)
    _x = "Y"
    while _x == "Y":
        for Class in dict1.keys():
            clear_screen()
            seatSales(Class)
            clear_screen()
        _x = typeIn(prawnz[6])
    sumList = listSum()
    profitList = list(map(lambda x,y:x*y, sumList, list(dict1.values())))
    indiceList = list(dict1.keys())
    clear_screen()
    for i in dict2.keys():
        print("\nTotal sales of ",sum(dict2[i]) , " Class ", i, "  tickets at $", dict1[i], " is $", format(profitList[indiceList.index(i)], ',.2f'), sep="")
    print("\nThe total sales for all ", sum(sumList), " tickets is $", format(sum(profitList), ',.2f'), ".", sep="")

                    #functions for example 12

                    # This is the main, and only, function for example
                    # 12. I could have used a seperate function for 
                    # finding the larger number, but the anonymous
                    # function seemed cleaner. So this clears the screen
                    # explains what it does, asks for two numbers,
                    # applies the max function which is using boolean
                    # math, then returns the larger number, with a 
                    # little excitement!
def ex_12():
    clear_screen()
    print(prawnz[7])
    sleep(1)
    first = typeIn(prawnz[8])
    second = typeIn(prawnz[9])
    maX = lambda x,y:(x>y)*x+(y>=x)*y
    d = maX(first,second)
    sleep(1)
    print("\n", d, "is WAY bigger!")

                    #functions for example 14

                    # This is the main function for example 14, and also
                    # uses some anonymous function condensation. It 
                    # clears the screen, explains itself, ask for input
                    # in kgs and m*s^-1, and calculates and outputs the
                    # total kinetic energy in joules.

def ex_14():
    clear_screen()
    print(prawnz[10])
    sleep(1)
    kinetic_energy = lambda m,v:1/2*m*v**2
    ke = kinetic_energy(numLoop(prawnz[11], insult[0]), numLoop(prawnz[12], insult[0]))
    sleep(1)
    print("\nThe kinetic energy of the object is", format(ke, ',.2f'), "Joules")

                    #Functions for exercise 15

                    # This is a simple function to calculate an average
def calc_average(list1):
    return sum(list1)/len(list1)
                    # This function returns a grade that corresponds to
                    # a user input score.
def determine_grade(score):
    if score < 60:
        return "F"
    if score in range(60,70,1):
        return "D"
    if score in range(70,80,1):
        return "C"
    if score in range(80,90,1):
        return "B"
    if score >= 90:
        return "A"
                    # This is the main function for exercise 15. It 
                    # clears the screen (for real programmers), explains
                    # itself (is that metaprograming?), it then asks for
                    # five test scores and adds them to a list. Finally
                    # it outputs each test score and it's corresponding 
                    # grade, then calculates and outputs the average 
                    # score and corresponding grade.
def ex_15():
    clear_screen()
    print(prawnz[13])
    list1 = []
    language = ["first", "second", "third", "fourth", "fifth"]
    list1.append(numLoop(prawnz[14], insult[0]))
    for i in range(4):
        list1.append(numLoop(prawnz[15], insult[0]))
    for i in range(len(list1)):
        print("Your", language[i], "test grade is", determine_grade(list1[i]))
        sleep(1)
    print("Your average score was", calc_average(list1), ".\n\nYour average grade is", determine_grade(round(calc_average(list1))) )
                    # This is just a function to create a list of error
                    # insults. more globals (sigh)
def _insult():
    global insult
    insult = ["\nThats not even a number.\n", "\nPlease input a number.\n"]
                    # The same as above but for various prompts to keep 
                    # the function definitions a little cleaner. I could
                    # have put all the text in here, but that seemed to
                    # make it less readable.
def _prompt():
    global prawnz
    prawnz = ["\nPlease select which programming exercise you would like to run, or q to exit:\n[1,7,12,14,15 or q]>  ", "This program will help you to convert kilometers to miles, becaus metric is the worst.\n", "Please enter the number of kilometers you would like to convert to miles.\n[Any number]>  ", """This program will help you to add up all your stadium ticket sales.\n\n\nThere are three classes of tickets.\n\n\nClass A tickets which cost $20.\n\n\nClass B tickets which cost $15.\n\n\nAnd class C which cost $10.\n\n\n\n\n\n\n\n\n\n\n\n\n""", "Please enter the number of class ", " tickets you sold.\n\n\n\n\n[number]>  ", "\n\n\nAre there any more ticket sales you would like to add?\n\n\n\n[Y or N]>  ", "This program utilizes a function to return the larger of two numbers,\nor the number itself if they are equal.\n", "\nPlease enter your first number.\n[Any Number]>  ", "\nPlease enter your second number.\n[Any number]>  ", "This program will calculate the kinetic energy of a moving object based on inputs of mass and velocity.", "\nPlease enter the objects mass in kilograms.\n[kg]>  ", "\nPlease enter the objects velocity in meters per second.\n[m*s^(-1)]>  ", "This program will calcusate grades for five test scores and the average score and grade.", "\nPlease enter your first test score.\n[score]>  ", "\nPlease enter the next test score.\n[score]>  "]
                    # This is the function for the main program loop
                    # allowing the user/teacher to select a programming
                    # exercise to run, or to exit the program.
def mainList():
    Class = "x"
    _insult()
    _prompt()
    ex_num = 'foo' 
    while ex_num != 'q': 
        print("\n")
        ex_num = typeIn(prawnz[0])
        if ex_num == 1:
            ex_1()
        elif ex_num == 7:
            ex_7()
        elif ex_num == 12:
            ex_12()
        elif ex_num == 14:
            ex_14()
        elif ex_num == 15:
            ex_15()

                    # run that puppy!
mainList()


















































































































































































































# This is all just a bit of fun, what the heck. 
# I couldn't figure out a better way to hide it without extra files,
# so it's hiding waaaay down here, and you just have to see it
# when you exit the main loop.

def oDD():
    print("\nI got in a fight with 1,3,5,7 and 9 ", end ="", flush=True)
    sleep(1.5)
    print("the odds were against me.")

def fight():
    print("\n19 and 20 had a fight...")
    sleep(1.5)
    print("21")

def un_luck():
    print("\nTHE NUMBER 13!?!?!?!?!?!?!?!?!")
    sleep(1.5)
    print("Not on my watch!")

def belt():
    print("\nWhat did 0 say to 8?")
    sleep(1.5)
    print("Nice belt!")

def really_sarcasm():
    print("\nWhy was 4 not impressed when 5 won a trophy for 6?")
    sleep(1.5)
    print("because,")
    sleep(1)
    print("\n511472")

def mel():
    print("""      Real Programmers write in FORTRAN.

     Maybe they do now,
     in this decadent era of
     Lite beer, hand calculators, and "user-friendly" software
     but back in the Good Old Days,
     when the term "software" sounded funny
     and Real Computers were made out of drums and vacuum tubes,
     Real Programmers wrote in machine code.
     Not FORTRAN.  Not RATFOR.  Not, even, assembly language.
     Machine Code.
     Raw, unadorned, inscrutable hexadecimal numbers.
     Directly.

     Lest a whole new generation of programmers
     grow up in ignorance of this glorious past,
     I feel duty-bound to describe,
     as best I can through the generation gap,
     how a Real Programmer wrote code.
     I'll call him Mel,
     because that was his name.

     I first met Mel when I went to work for Royal McBee Computer Corp.,
     a now-defunct subsidiary of the typewriter company.
     The firm manufactured the LGP-30,
     a small, cheap (by the standards of the day)
     drum-memory computer,
     and had just started to manufacture
     the RPC-4000, a much-improved,
     bigger, better, faster --- drum-memory computer.
     Cores cost too much,
     and weren't here to stay, anyway.
     (That's why you haven't heard of the company,
     or the computer.)

     I had been hired to write a FORTRAN compiler
     for this new marvel and Mel was my guide to its wonders.
     Mel didn't approve of compilers.

     "If a program can't rewrite its own code",
     he asked, "what good is it?"

     Mel had written,
     in hexadecimal,
     the most popular computer program the company owned.
     It ran on the LGP-30
     and played blackjack with potential customers
     at computer shows.
     Its effect was always dramatic.
     The LGP-30 booth was packed at every show,
     and the IBM salesmen stood around
     talking to each other.
     Whether or not this actually sold computers
     was a question we never discussed.

     Mel's job was to re-write
     the blackjack program for the RPC-4000.
     (Port?  What does that mean?)
     The new computer had a one-plus-one
     addressing scheme,
     in which each machine instruction,
     in addition to the operation code
     and the address of the needed operand,
     had a second address that indicated where, on the revolving drum,
     the next instruction was located.

     In modern parlance,
     every single instruction was followed by a GO TO!
     Put *that* in Pascal's pipe and smoke it.

     Mel loved the RPC-4000
     because he could optimize his code:
     that is, locate instructions on the drum
     so that just as one finished its job,
     the next would be just arriving at the "read head"
     and available for immediate execution.
     There was a program to do that job,
     an "optimizing assembler",
     but Mel refused to use it.

     "You never know where it's going to put things",
     he explained, "so you'd have to use separate constants".

     It was a long time before I understood that remark.
     Since Mel knew the numerical value
     of every operation code,
     and assigned his own drum addresses,
     every instruction he wrote could also be considered
     a numerical constant.
     He could pick up an earlier "add" instruction, say,
     and multiply by it,
     if it had the right numeric value.
     His code was not easy for someone else to modify.

     I compared Mel's hand-optimized programs
     with the same code massaged by the optimizing assembler program,
     and Mel's always ran faster.
     That was because the "top-down" method of program design
     hadn't been invented yet,
     and Mel wouldn't have used it anyway.
     He wrote the innermost parts of his program loops first,
     so they would get first choice
     of the optimum address locations on the drum.
     The optimizing assembler wasn't smart enough to do it that way.

     Mel never wrote time-delay loops, either,
     even when the balky Flexowriter
     required a delay between output characters to work right.
     He just located instructions on the drum
     so each successive one was just *past* the read head
     when it was needed;
     the drum had to execute another complete revolution
     to find the next instruction.
     He coined an unforgettable term for this procedure.
     Although "optimum" is an absolute term,
     like "unique", it became common verbal practice
     to make it relative:
     "not quite optimum" or "less optimum"
     or "not very optimum".
     Mel called the maximum time-delay locations
     the "most pessimum".

     After he finished the blackjack program
     and got it to run
     ("Even the initializer is optimized",
     he said proudly),
     he got a Change Request from the sales department.
     The program used an elegant (optimized)
     random number generator
     to shuffle the "cards" and deal from the "deck",
     and some of the salesmen felt it was too fair,
     since sometimes the customers lost.
     They wanted Mel to modify the program
     so, at the setting of a sense switch on the console,
     they could change the odds and let the customer win.

     Mel balked.
     He felt this was patently dishonest,
     which it was,
     and that it impinged on his personal integrity as a programmer,
     which it did,
     so he refused to do it.
     The Head Salesman talked to Mel,
     as did the Big Boss and, at the boss's urging,
     a few Fellow Programmers.
     Mel finally gave in and wrote the code,
     but he got the test backwards,
     and, when the sense switch was turned on,
     the program would cheat, winning every time.
     Mel was delighted with this,
     claiming his subconscious was uncontrollably ethical,
     and adamantly refused to fix it.

     After Mel had left the company for greener pa$ture$,
     the Big Boss asked me to look at the code
     and see if I could find the test and reverse it.
     Somewhat reluctantly, I agreed to look.
     Tracking Mel's code was a real adventure.

     I have often felt that programming is an art form,
     whose real value can only be appreciated
     by another versed in the same arcane art;
     there are lovely gems and brilliant coups
     hidden from human view and admiration, sometimes forever,
     by the very nature of the process.
     You can learn a lot about an individual
     just by reading through his code,
     even in hexadecimal.
     Mel was, I think, an unsung genius.

     Perhaps my greatest shock came
     when I found an innocent loop that had no test in it.
     No test.  *None*.
     Common sense said it had to be a closed loop,
     where the program would circle, forever, endlessly.
     Program control passed right through it, however,
     and safely out the other side.
     It took me two weeks to figure it out.

     The RPC-4000 computer had a really modern facility
     called an index register.
     It allowed the programmer to write a program loop
     that used an indexed instruction inside;
     each time through,
     the number in the index register
     was added to the address of that instruction,
     so it would refer
     to the next datum in a series.
     He had only to increment the index register
     each time through.
     Mel never used it.

     Instead, he would pull the instruction into a machine register,
     add one to its address,
     and store it back.
     He would then execute the modified instruction
     right from the register.
     The loop was written so this additional execution time
     was taken into account ---
     just as this instruction finished,
     the next one was right under the drum's read head,
     ready to go.
     But the loop had no test in it.

     The vital clue came when I noticed
     the index register bit,
     the bit that lay between the address
     and the operation code in the instruction word,
     was turned on ---
     yet Mel never used the index register,
     leaving it zero all the time.
     When the light went on it nearly blinded me.

     He had located the data he was working on
     near the top of memory ---
     the largest locations the instructions could address ---
     so, after the last datum was handled,
     incrementing the instruction address
     would make it overflow.
     The carry would add one to the
     operation code, changing it to the next one in the instruction set:
     a jump instruction.
     Sure enough, the next program instruction was
     in address location zero,
     and the program went happily on its way.

     I haven't kept in touch with Mel,
     so I don't know if he ever gave in to the flood of
     change that has washed over programming techniques
     since those long-gone days.
     I like to think he didn't.
     In any event,
     I was impressed enough that I quit looking for the
     offending test,
     telling the Big Boss I couldn't find it.
     He didn't seem surprised.

     When I left the company,
     the blackjack program would still cheat
     if you turned on the right sense switch,
     and I think that's how it should be.
     I didn't feel comfortable
     hacking up the code of a Real Programmer.""")


def fun_functions():
    print("Do you want to hear a joke?")
    jokes = ["oDD()", "fight()", "un_luck()", "belt()", "really_sarcasm()", "mel()"]
    global yn
    yn = 0
    while True:
        yn = input("[Y,N,maybe, I'm not really sure, absolutely!!!, Mel]>  ")
        if yn == "Mel":
            mel()
            break
        elif yn in ["Y","maybe", "I'm not really sure", "absolutely!!!"]:
            eval(jokes[random.randint(0,5)])
            break
        elif yn == "N":
            print("fine then")
            break
    if yn not in ["Y","maybe","I'm not really sure", "absolutely!!!", "Mel"]:
        print("Can't even take jokes seriously huh?")

fun_functions()





