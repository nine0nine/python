#Caspian Baskin
#EGR 115
#HW2
#Jan,28,2019







#Initialise list structure. 
#I read ahead a bit. I have some limited experience with programming 
#languages and figured this structure would be useful throughout the class
#let me know if it's inappropriate

#Prompt for main Home Work list
prompt = "Please select which programming exercise you would like to run, 1,7,11,13,15 or q to exit\n>  "         

#Initialize list input variable to something other than the value in while statement                            
ex_num = 'foo'                                                                                                   

#start main list loop
while ex_num != 'q':                                                                                                                       
    print("\n")

    #ask for main list input

    ex_num = input(prompt)                                                                                                                  

    #A program that displays the day of the week that corresponds to an input number

    if ex_num == '1':                                                                                                                       
        dow = """\nPlease enter a number from 1 to 7 or q to quit\n>  """#prompt variable
        dow_list = [ "Foo", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ] #Initialize a list with all the days of the week and a dummy variable in spot 0 so input question sounds reasonable
        downum = int(input(dow)) #get input with prompt variable
        day = dow_list[downum] #get day from list
        print("\nYou have selected ", day, """!!! a splendid choice.""", sep='') #display the selected day

    #A program that asks for one of three selectable primary colors and displays what they make when mixed and give errors otherwise

    elif ex_num == '7':                                                 
        col1 ='foey' #first color
        col2='bar' #second color
        mix = "\nYour colors mixed together make," #answer message
        colist = ["foo", "red", "yellow", "blue", "azure", "brown"] #list of acceptable inputs, dummy variable to fill spot 0 and 4 so i can use some math, and a joke
        while col1 not in colist or col2 not in colist or col1 == col2: #Start a prompt loop
            col1=input("\nPlease enter two primary colors: red yellow blue:\n>  ") #prompt for input
            col2=input(">  ")
            if col1 not in colist or col2 not in colist: #determine if user followed directions
                print("\nERR:[error 287351] It needs to be either red, yellow or blue.     Or else...") #display error message and threaten user
            if col1 == col2: #determine if user entered the same color twice
                print("\nERR:[error 2] terminal error: user not following directions") #display error message and insult user
        if col1 in colist and col2 in colist: 
            indects = colist.index(col1) + colist.index(col2) #do some fancy math with color index numbers
            if indects == 3: #determine if input colors were red and yellow
                print(mix, "orange!!!") #response
            if indects == 4: #determine if they were red and blue
                print(mix, "purple!!!") #response
            if indects == 5: #determine if they were yellow and blue
                print(mix, "green!!!") #response
        poolist = [ col1, col2 ] #create unnecessary list to keep using lists for everything even though this chapter was about something else
        if "brown" in poolist: #determine if one of the input colors was brown
            print(mix, "more brown!!!!!") #response
       
    #A program for a book store that takes a customers total books bought number and tells them how many bonus points they have earned

    elif ex_num == '11':
        booknum = int(input("\nPlease enter the number of books you have purchased.\n>  ")) #Prompt variable
        def ans(): #create unnecessary function to select a display style for the answer, despite the fact that this chapter is not about functions 
            if booknum % 2 == 1 and booknum < 8: #determine if the number is odd and within the range of increasing points
                print("\nYou have earned", pointnum, "points. Just one more book to the next point level!!!") #response
            elif booknum == 0: #is it 0?
                print("\nYou have earned", pointnum, "points and...\nI'm not really sure why you bothered, \nbut there it is. \nenjoy.") #response
            elif booknum > 8: #is it bigger than the range of increasing point levels?
                print("\nYou have earned the maximum,", pointnum, "points") #response
            else: #if not any of these conditions
                print("\nYou have earned", pointnum, "points!") #response
        if booknum < 2: #determine where in the range of increasing point levels the number is and assign the corresponding amount of points
            pointnum = 0
        elif booknum in [2,3]:
            pointnum = 5
        elif booknum in [4,5]:
            pointnum = 15
        elif booknum in [6,7]:
            pointnum = 30
        elif booknum >= 8:
            pointnum = 60
        ans() #use my fancy function to display the answer
    
    #A program to calculate the shipping cost of a package based on weight

    elif ex_num == '13':
        print("\nWelcome to the Fast Freight shipping company!") #a friendly greeting to soothe users
        pacweight = float(input("Please enter the weight, in pounds, of your package to calculate shipping costs\n>   ")) #ask for weight in pounds force
        price = [1.5,3.00,4.00,4.75] #create a list of the different prices, even though the chapter is about decision making
        price1 = list(range(1,3,1)) #create another list with the range for the lowest price in integers
        price2 = list(range(3,7,1)) #and another list for the second range, there will be some decision making soon
        price3 = list(range(7,11,1)) #after one last list for the second highest price range
        nn = round(pacweight+.5) #round all inputs up to the nearest integer. This is the only part I didn't get from the book
        if nn in price1: #make a decision about which range of pricing the input weight falls into and store the final price and price per pound
            myprice = price[0]
            pacprice = pacweight * myprice
        elif nn in price2:
            myprice = price[1]
            pacprice = pacweight * myprice
        elif nn in price3:
            myprice = price[2]
            pacprice = pacweight * myprice
        else:
            myprice = price[3]
            pacprice = pacweight * myprice
        print("\nThe price for shipping your package is, $", format(pacprice, '.2f'), ".\nCost per pound is, $", format(myprice, '.2f'), ". \nHave a nice day!", sep='') #display the response with impeccable formatting

    #A program to convert seconds to years, days, hours and minutes

    elif ex_num == '15':
        def daytime(num): #a function that does the thing what needs doing and stores the numbers of each unit in global variables
            global years
            global days
            global hours
            global minutes
            global secs
            years = num // 31536000 #divide the input number and discard the remainder
            rem0 = num % 31536000 #store the remainder and repeat the process
            days = rem0//86400
            rem1 = rem0 % 86400
            hours = rem1 // 3600
            rem2 = rem1 % 3600
            minutes = rem2 // 60
            secs = rem2 % 60
        daytime(int(input("\nEnter the number of seconds you would like to convert to days hours minutes and seconds\n>   "))) #prompt for a number in seconds and input to my awesome function
        if years not in [0,1]: #if there are multiple years then print them to the screen
            print(years, "years", end=' ')
        if days not in [0,1]: #the same for days
            print(days, "days", end=' ')
        if hours not in [0,1]: #the same for hours
            print(hours, "hours", end=' ')
        if minutes not in [0,1]: #the same for minutes
            print(minutes, "minutes", end=' ')
        if secs not in [0,1]: #the same for seconds
            print(secs, "seconds")
        if years == 1: #if there's only one year print it to the screen with the proper language
            print(years, "year", end=' ')
        if days == 1: #and for days
            print(days, "day", end=' ')
        if hours == 1: #and for hours
            print(hours, "hour", end=' ')
        if minutes == 1: #and for minutes
            print(minutes, "minute", end=' ')
        if secs == 1: #and for seconds
            print(secs, "second")
