from time import sleep

xMin = float(input( "Enter min x value."))
xMax = float(input( "Enter max x value."))
yMin = float(input( "Enter min y value."))
yMax = float(input( "Enter max y value."))
stepVal = float(input( "Enter a step value. "))

print( "X", "Y", "Z", sep="\t")
def drange(a,b,c):
    p=a
    while True:
        if p >=b:
            return
        yield p
        p+=c


for x in drange(xMin,xMax,stepVal):
    for y in drange(yMin, yMax,stepVal):
        z = -x**2 - y**2 + 6
        print(format({,.2f}.format(x,y,z),sep="\t")
        sleep(.4)
