#Project 2
#Cas Baskin

     #  ******   **                           **   **                  
     # /*////** //                           /**  //                   
     # /*   /**  **  ******  *****   *****  ****** **  ******  ******* 
     # /******  /** **////  **///** **///**///**/ /** **////**//**///**
     # /*//// **/**//***** /*******/**  //   /**  /**/**   /** /**  /**
     # /*    /**/** /////**/**//// /**   **  /**  /**/**   /** /**  /**
     # /******* /** ****** //******//*****   //** /**//******  ***  /**
     # ///////  // //////   //////  /////     //  //  //////  ///   // 
     #  *******                       **  
     # /**////**                     /**  
     # /**   /**   ******   ******  ******
     # /*******   **////** **////**///**/ 
     # /**///**  /**   /**/**   /**  /**  
     # /**  //** /**   /**/**   /**  /**  
     # /**   //**//****** //******   //** 
     # //     //  //////   //////     //  
     #   ********           **                        
     #  **//////           /**                        
     # /**         ******  /** **    **  *****  ******
     # /********* **////** /**/**   /** **///**//**//*
     # ////////**/**   /** /**//** /** /******* /** / 
     #        /**/**   /** /** //****  /**////  /**   
     #  ******** //******  ***  //**   //******/***   
     # ////////   //////  ///    //     ////// ///    



                    # Input function, takes a prompt string, and returns
                    # typed output of either Int64 Float64 or String. Can
                    # be extended at some point to other types if
                    # necessary.
function typeIn(prom::String):Any
    print(prom)
    w =chomp(readline())
    i=tryparse(Int64, w)
    f=tryparse(Float64, w)
    if typeof(f) ==Float64
        if typeof(i) == Int64
            return i
        end
    return f
    else
        return w
    end

end
                    # Loop that also takes a prompt and an
                    # insult/guidance phrase. It loops over using the
                    # previous function and providing guidance, and only returns if the input is
                    # a number.
function numLoop(prompt::String="", insult::String="")::Number
    while true
        n=typeIn(prompt)
        if typeof(n) <: Number
            return n
            break
        else
            println(insult)
        end
    end
end
lang = ""

                    # Function for getting appropriate c value. Accepts
                    # only numeric input then calculates complex roots
                    # and checks imaginary part to see if the function
                    # has real roots. then calculates the c value for
                    # which the function has one real root. Then it
                    # defines language for whether appropriate values are
                    # larger or smaller than that value based on the sign
                    # of a. and asks for a new c value if neccesary.
function cLoop()
    while true
        global c = typeIn("""\nPlease enter in the last coefficient, coefficient "c" \n[Integer]>  """)
        if typeof(c) <: AbstractString
            print("\nNumbers only please.\n")
            continue
        end
        d()= sqrt(Complex((-c/a))+((b/a)/2)^2)-((b/a)/2)
        e()= -sqrt(Complex((-c/a))+((b/a)/2)^2)-((b/a)/2)
        if imag(d()) != 0
            solut = a*((b/(2*a))^2)
            if a<=0
                lang = "greater"
            end
            if a>=0
                lang = " less than"
            end
            sleep(1)
            println("\nUnfortunatly that quadratic function only has complex zeros. Please enter a number for c$lang $(round(solut, digits=5)).")
        end
        if imag(d()) == 0
            return c
            break
        end
    end
end

                    # Another looping function to get a canverging value
                    # for tho lower end of the range. Alot of this stuff
                    # can be cleaned up but it works for now. This asks
                    # for a lower range starting point and then checks it
                    # against known roots. it will return two values if
                    # it is at least smaller than the larger root (here
                    # D). If the range value is smaller than the smaller
                    # root it returns tha value and group number 1. if
                    # its smaller than the larger root but greater than
                    # the smaller root it returns the value and a group
                    # number 2.
function rangeLow(prompt::String="")
    while true
        r = numLoop(prompt, "\nIt needs to be a number.")
        if r <= max(D,E)
            if r <= min(D,E)
                return (r, 1)
            else
                return (r, 2)
            end
            break
        else
            println("\nLets try something smaller than")
            sleep(.75)
            println("say")
            sleep(.75)
            println("$(round(max(D,E),digits=5)).")
        end
    end
end
                    # Another looping function for the high range value.
                    # It checks the group number returned from the
                    # previous function to determine which section of the
                    # function this value needs to be in. for group
                    # number 1 the higher range value needs to be between
                    # the two roots. for group number two it needs to be
                    # larger than the higher root. Also its important to
                    # note if f(x) only has one real root only that
                    # precise root number will be accepted for both range
                    # values. This is strange behavior, but no errors!
function rangeHigh(prompt::String="")::Number 
    while true
        R = numLoop(prompt, "\nIt needs to be a number.")
        if group == 1
            if max(D,E) >= R >= min(D,E)
                return R
                break
            end
            if R < min(D,E)
                println("\nPerhaps something larger than $(round(min(D,E), digits=5)).")
            end
            if R > max(D,E)
                println("\nMaybe something smaller than $(round(max(D,E),digits=5)).")
            end
        end
        if group == 2
            if min(D,E) >= R || R >= max(D,E)
                return R
                break
            else
                println("\nI think you should try something bigger than $(round(max(D,E), digits=5)) or smaller than $(round(min(D,E), digits=5)), y'know if you want this thing to wark and all...")
            end
        end
    end
end


                    # Main body of the program again its pretty messy but
                    # functional. Hopefully I can clean this up soon and
                    # get global variables all inside functions or all
                    # passed as arguments to functions so julias LLVM
                    # knows what theyre all about. this first line
                    # explains the basics, then we ask for a and b
                    # which will all be correct input due to error
                    # checking.
println("""\nThis program will calculate the roots of a quadratic function of the form "ax^2+bx+c" """)
a = numLoop("""\nPlease enter in the first coefficient, coefficient "a" \n[Integer]>  """, "\nNumbers only please.")
b = numLoop("""\nPlease enter in the second coefficient, coefficient "b" \n[Integer]>  """, "\nNumbers only please.")

                    # redefine complex roots in the global scope. I dont
                    # remember why I had to do this but hopefully the
                    # complex argument types them well enough to be
                    # performant.
d()= sqrt(Complex((-c/a))+((b/a)/2)^2)-((b/a)/2)
e()= -sqrt(Complex((-c/a))+((b/a)/2)^2)-((b/a)/2) 
                    # get c
c = cLoop()
                    # define the real roots now that we have appropriate
                    # coefficients. theyre constants so the LLVM can
                    # staticaly type them. This works well when running
                    # as a script but the REPL will complain but not
                    # error if you run it multiple times there. fyi
const D=real(d())
const E=real(e())
                    # get low and high range values r,R 
r,group = rangeLow("\nPlease enter the lower bound of the range to find a zero in.\n[Number]>  ")
R = rangeHigh("\nPlease enter the upper range value.\n[Number]>  ")
println("\nPlease enter the convergence criteria you would like to use to find a root. Something between 1 and 1.0e-16 is recommended. The smaller the number is the more accurate the result at the cost of computing time.")
sleep(1)
                    # get desired precision note were in julia so input
                    # can be in e notation, I.E. 1e-14, i think using f
                    # notation will error, or it might not. But it
                    # certainly wont get you very precise number.
err = numLoop("\n\nAs in I would recommend something larger than 0.0000000000000001, 1.0e-12 is probably perfect. \n[Number]>  ")
                    # define the actual polynomial. This illustrates one
                    # of the main reasons I like julia because this type
                    # of definition is almost exactly what i would write
                    # on paper.
f(x) = a*x^2+b*x+c

                    # The main actian function. Takes the twe numbers
                    # defining the range, finds the mid point between
                    # them. Then checks the sign value of that point as
                    # the functions input. it then replaces the range
                    # falue whose f() output has the matching sign. it
                    # then subtracts the twe range values and compares
                    # the result to the precision value, if the precision
                    # is larger, the loop starts over. If its smaller the
                    # two range values are returned as a tuple. I chaged
                    # this at the last second so that the while loop
                    # compares the value f(l) to error instead of h-l,
                    # this seems to get even better precision with little
                    # cost to the time.
function bisect(x,y)
    l=x
    h=y
    while abs(f(l)) > err
        m = (h + l)/2
        if sign(f(m)) == sign(f(l))
            l = m
        else
            h = m
        end
    end
    return l,h
end
                    # This runs the main function along with the @timed
                    # macro which outputs a tuple including the values of
                    # the function, the run time of the function memory
                    # allocation and what not. I included it here in the
                    # hopes of having another algorithm as well and then
                    # they could race. but I think I may be out of time.
tim=@timed bisect(r,R)
                    # show the selected quadratic.
println("\nFor the function,\nf(x) = ($a)x^2+($b)x+($c)")
                    # show the selected search range and tolerance.
println("With a search range of [$r to $R].\nAnd a tolerance of $err,")
                    # show the final range values.
println("The range is [ $(tim[1][1]) to $(tim[1][2]) ].")
println("The final estimate is $((tim[1][1]+tim[1][2])/2).")
                    # show the time it took to run the bisect function,
                    # please note that this time will dramatically
                    # decrease on a second run if everything else stayed
                    # the same.
println("Calculated in a time of $(tim[2]) seconds.")






# function diff(f,x,h=1e-14::Float64)
#     return (f(x+h)-f(x))/h
# end

#function dekk(x,y)
#    l=x
#    h=y
#    _i=l




