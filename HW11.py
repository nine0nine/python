#Caspian Baskin
#EGR 115
# HW11
# Today

import os
# Sorry I just realised that this was due today. I had planned on adding the comments tomorrow. This is based on a command line program I wrote for myself after watching your video. Although that one uses arguments passed on the initial command line to process the directory and search string. I think this is pretty conceptually similar to the program in the video.
print("This program will either help you find files with a certain sub string in them, or print all files found in all sub directories of a chosen root directory.\n\n")
while True:
    _dir = input("Please enter the name of the directory to search.\n[valid dir]>  ")
    if os.access(_dir, os.F_OK) == True:
        break
    else:
        print("\nPlease enter a valid directory pathname.\n")


search = input("Please enter a string to search the filenames for, or empty to see all files in all sub directaries.\n[search string or enter]>  ")

if search != "":
    x=[r+"/"+f for r,ds,fs in os.walk(_dir, followlinks=True) for f in fs if search in f]
    for i in x:
        print(i)
else:
    x=[r+"/"+f for r,ds,fs in os.walk(_dir, followlinks=True) for f in fs]
    for i in x:
        print(i)
