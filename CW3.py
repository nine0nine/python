name1 = 'Mary'
name2 = 'Mark'
if name1 == name2:
    print('The names are the same.')
else:
    print('The names are not the same.')



password = input('Enter the password: ')

if password == 'trystero':
    print('Password accepted.')
else:
    print('Sorry, that is wrong. this computer will self destruct in 5 seconds')

score = float(input('Enter your test score: '))

if score >= 90:
    print('Your grade is A.')
elif score >= 80:
    print('Your grade is B.')
elif score >= 70:
    print('Your grade is C.')
elif score >= 60:
    print('Your grade is D.')
else:
    print('Your grade is F.')
