#Caspian Baskin
#EGR 115
#HW1
#Jan,28,2019







#Initialise list structure. 
                                                            #I read ahead a bit. I have some limited experience with programming 
                                                            #languages and figured this structure would be useful throughout the class
                                                            #let me know if it's inappropriate

prompt = "Please select which programming exercise you would like to run, 1,2,4,9,10 or q to exit: "                                        #Prompt for main HW list
ex_num = 'foo'                                                                                                                              #Initialize list input variable to something other than the value in while statement
while ex_num != 'q':                                                                                                                        #start main list loop
    print("\n")                     
    ex_num = input(prompt)                                                                                                                  #ask for main list input

    if ex_num == '1':                                                                                                                       #determine if string 1 has been selected and execute code block if so
                                                                                                                    #Programming exercise 1: print personal information for all to see!
        print("\n" +
              "Caspian Baskin \n \n" +
              "10 Heather ct, Woodbury, CT, 06798 \n \n" +
              "(828)593-9455 \n \n " +
              "Engineering Science","\n\n")

    elif ex_num == '2':                                                                                                                     #determine if string 2 has been selected
                                                                                                                    #Programming exercise 2: show projected profits
        print("\n")
        total_sales = float(input("Please enter projected annual sales to display projected annual profits: "))                             #assign projected profits based on user input
        profit = total_sales * 0.23                                                                                                         #calculate profits
        print("\n","Projected annual profit is: $", format(profit, ',.2f'),"\n\n", sep='')                                                  #print formatted projected annual profit


    elif ex_num == '4':                                                                                                                     #determine if string 4 is selected
                                                                                                                    #Programming exercise 4: calculate sales total and tax at 7%
        sales_prompt = """Please input item prices followed by "=" when done: """                                                           #prompt variable
        print("\n") 
        number = 0.0                                                                                                                        #initialize variable as a float
        total = 0.0                                                                                                                         #initialise accumulating variable
        while True:                                                                                                                         #start a loop
            number = input(sales_prompt)                                                                                                    #request a price (will be a string at this point
            try:                                                                                                                            #attempt to define input as a float
                floating = float(number)
                total = total + floating                                                                                                    #if successful add float to total
            except ValueError:                                                                                                              #if float() throws a ValueError which will happen if input string is NaN
                print("The total plus sales tax is: $", format(total * 1.07, ',.2f'),  sep='')                                              #then show total plus sales tax at 7% and end program
                break
    
    elif ex_num == '9':                                                                                                                     #determine if string 9 is selected
                                                                                                                    #Programming exercise 9: convert Celsius to Fahrenheit
        temp_prompt = "Please enter a temperature in degrees Celsius: "                                                                     #prompt for temp conversion
        print("\n")
        print("Celsius to Fahrenheit Converter")                                                                                            #title? I dunno what the heck
        print("\n")
        tempC = float(input(temp_prompt))                                                                                                   #Celsius variable
        tempF = 9/5 * tempC + 32                                                                                                            #conversion to Fahrenheit and storage in variable
        print("Temperature in Fahrenheit", tempF, "degrees.")                                                                               #print results

    elif ex_num == '10':                                                                                                                    #determine if string is 10
                                                                                                                    #Programming exercise 10: take a recipe for 48 cookies and calculate ingredient amounts for any amount of cookies
        print("\n")
        cookie_prompt = "Please input the number of cookies you would like to bake: "                                                       #cookie prompt
        sug = 1.5                                                                                                                           #store recipe
        butt = 1.0                                                                        
        flo = 2.75
        sug_per = sug / 48.0                                                                                                                #determine amount per one cookie
        butt_per = butt / 48.0
        flo_per = flo / 48.0
        cookies = float(input(cookie_prompt))                                                                                               #ask user for desired amount of cookies
        print("\n" +                                                                                                                        #show necessary amount of ingredients for desired amount of cookies
              format(sug_per * cookies, '.1f') +                                        
              " cups of sugar" + 
              "\n", "\n" + 
              format(butt_per * cookies, '.1f') + 
              " cups of butter" + 
              "\n", "\n" + 
              format(flo_per * cookies, '.1f') + 
              " cups of flour")
