#Caspian Baskin
#EGR 115
#HW5
#March, 6th, 2019

                # Import some usefull functions, OrderedDict to make 
                # the code backwards compatible with older versions 
                # of python that didn't preserve dict order, and, 
                # sleep to really draw this whole thing out.
from collections import OrderedDict
from time import sleep
                # This is my new input function. I came up with this in 
                # the hopes of making future input error checking easier
                # and less verbose. typeInput() will return a string if 
                # the input is not a number, a float if there is a 
                # fractional part, and an int if it's a whole number. My 
                # hope is to avoid the use of switch variables and have
                # cleaner error checking loops without intrducing a 
                # million new variables.
def typeInput(promp):
    stuff = input(promp)
    x = "x"
    try:
        x = float(stuff)
        if x%1 == 0:
            x = int(stuff)
    except ValueError:
        return stuff
    if type(x) != str:
        return x

                # main exercise selection prompt
promptMain = "Please select which programming exercise you would like to run, 5,15 or q to exit: "
        
                # Initialize list input variable to something other than
                # the value in while statement
ex_num = 'foo'
                # Start the main loop
while ex_num != 'q':
    print("\n")
                # Ask for main list input, no longer returns a string 
                # unless "q"
    ex_num = typeInput(promptMain)

                # This is a programming exercise that will take input in 
                # the form of number of years and the number of inches 
                # of rainfall during each month of each year. Then 
                # outputs the total rainfall over the selected period of
                # time (rounded down whole number if input is a floating
                # point number) as well as the averaged rainfall in feet
                # and inches over the selected period of time. I think I
                # like the way I did this but I also feel like there 
                # must be an easier way to get values out of multiple 
                # dictionaries.
    if ex_num == 5:
                # Prompt for time span in years
        yearsPrompt = ("Please enter the number of years for which you would like" +
                                                " to calculate average rainfall.\n>  ")
        yearsNum = typeInput(yearsPrompt)
                # Create an OrderedDict linguistic keys pointing to the 
                # months of the year
        months = OrderedDict([('first', 'January'),
                              ('second', 'February'),
                              ('third', 'March'),
                              ('fourth', 'April'),
                              ('fifth', 'May'),
                              ('sixth', 'June'),
                              ('seventh', 'July'),
                              ('eigth', 'August'),
                              ('ninth', 'September'),
                              ('tenth', 'October'),
                              ('eleventh', 'November'),
                              ('twelfth', 'December')])
                # pull month values into a list for less messy for 
                # statements
        month = list(months.values())
                # create a new OrderedDict with the names of the months 
                # as keys pointing to empty lists.
        rainz = OrderedDict()
        for a in month:
            rainz[a] = [] 
                # Prompt for measured rainfall per month per year.
        monthPrompt = "Please enter the total rainfall for "
                # Nested loop structure (as requested) to get rainfall 
                # for each month and add it to the corrosponding 
                # dictionary key's list in the index that corresponds to
                # the year. Probably uneccesary but it seems better to 
                # me, more scaleable and robust, and could theoretically
                # answer more questions than the ones required. 
                # Start loop for number of years.
        for i in range(yearsNum):
                # Start loop for months to get each data point
            for a in month:
                # Steal the corresponding list as it is now. Empty list 
                # on the first run.
                x = rainz[a]
                # Start error checking loop to make sure we have useable
                # data points. If not, brief insult and ask again. Not 
                # taking advantage of my input function, oh well...
                while True:
                # Ask for a number and convert it to float and append it
                # at the end of our stolen list. This is where errors 
                # will show up if user inputs a string or something.
                    try:
                        x.append(float(input("Please enter the rainfall for the "               + 
                                        list(months.keys())[month.index(a)]                     + 
                                        " month (" + a + ") for the "                           + 
                                        list(months.keys())[i]                                  + 
                                        " year, in inches.\n>  ")))
                # Give the list back with extras!
                        rainz[a] = x
                # Move on
                        break
                    except ValueError:
                # If the above float conversion throws an error insult 
                # and return to the beginning of the loop.
                        print("\nYou need to enter a number.")
                # Print uselessly proportional pacman progress bar that 
                # probably uses more cpu time than the actual 
                # calculations.    
        for i in range(yearsNum*10 + 1 ):
            sleep(0.5)
            if i % 2 == 0:
                print("-"*i, "C", "o " * (int(yearsNum)*5 - i), flush=True, sep='', end='\r')
            if i % 2 == 1:
                print("-"*i, "c", " o" * (int(yearsNum)*5 - i), flush=True, sep='', end='\r')

                # Create yet another list whose contents are the averages
                # of each months rainfall values for all years. Indexed 
                # identically to the months they correspond to. I used 
                # list comprehension here which I'm not sure is in the 
                # book but I think it looks much nicer on the page and 
                # helps with not having to fill a million variables with
                # a million lists
        avgz = [sum(rainz[i]) / len(rainz[i]) for i in month]
                # Do the same for the total rainfall
        totalInsh = sum([sum(rainz[i]) for i in month])
        
                # Define two functions to break the inches into feet and
                # remaining inches.
        def feat(ins):
            return ins//12
        
        def inch(ins):
            return ins % 12
                # Output the total rainfall using another list compre-
                # hension. 
        print("\n\nThe total rainfall over the ", 
                # Sum of a list of the lengths of all the month values 
                # each of which stands for one month, overly complex but
                # would be usefull for nonstandard collections of data
                sum([len(rainz[i]) for i in month]),
                " month period was ", end = "")
                # If there's enough to consolidate inches to feet show 
                # feet.
        if feat(totalInsh) != 0:print(format(feat(totalInsh), ",.2f"), "feet", end="")
                # If there are any inches left over show them.
        if inch(totalInsh) != 0:print(inch(totalInsh), "inches", end="")
        print("\n")
                # dramatic pause
        sleep(1)
                # For loop to; explain, identify and print the average 
                # for each month over the specified range of years.
        for i in range(len(avgz)):
            print("\nThe average rainfall for the month of ", 
                    month[i], " over ", yearsNum, " years is, ")
                # Comprehension pause?
            sleep(.7)
            print("\n    ", month[i]) 
                # again don't print feet or inches unless there are some.
            if feat(avgz[i]) != 0:print(format(feat(avgz[i]), ',.2f'), "feet", format(inch(avgz[i]), ',.2f'),  " inches")
            if feat(avgz[i]) == 0:print(format(inch(avgz[i]), ',.2f'),  " inches")
                # Alexa, end program.





                # This exercise is to print an arbitrary shape defined in
                # the book as,
                # 
                #           ##
                #           # #
                #           #  #
                #           #   #
                #           #    #
                #           #     #
                #
                #
                #      Booooooring. Oh well here we go...
    elif ex_num == 15:
                # nested loops to print the dumb thing
        for i in ([("#" + " " * i + "#" ) for i in range(6)]):
            print(i)




























        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
                # Ha! I knew I could make it better!
        sleep(3)
        for a in range(6):
            for i in ([(("#" + " " * i + "#" ) + " "*(5-i+(a*6)) + ("#" + " " * i + "#" )) for i in range(6)]):
                print(i, flush=True)
                sleep(.1)

        print("\n\n\nOh...", flush=True, end="")
        sleep(1)
        print("       No!", flush=True, end="") 
        sleep(1.6) 
        for i in list("                         The Christmas TreeeeeeeeeEEE !!!!!!!!"):
            print(i,flush=True,end="")
            sleep(.2)
                # Really dramatic pause
        sleep(5)

