#Caspian Baskin
#EGR 115
#HW8
#beginning of April
                        # Import some fancy stuff.            
from time import sleep
from collections import OrderedDict
import re
from string import ascii_lowercase as letters
import os
                    # I think you already know what this is all
                    # about.
def numLoop(_prom, _insult):
    while True:
        x = typeIn(_prom)
        if type(x) == int or type(x) == float:
            return x
            break
        elif x == "":
            return x
            break
        else:
            print(_insult)
                    # And this one.
def strLoop(prom, insult):
    while True:
        x = typeIn(prom)
        if type(x) == str:                                              
            return x                                                 
            break
        else:
            print(insult)
                    # This is a slightly modified string only loop that
                    # only accepts certain inputs, which you need to give
                    # to the function in a list.
def strictStrLoop(_prom, _insult, _list):
    while True:
        x = typeIn(_prom)
        if type(x) == str and x in _list:
            return x
            break
        else:
            print(_insult)
                    # This is the same.
def typeIn(promt):
    stuff = input(promt)
    try:
        stuff = float(stuff)
        if stuff%1 == 0:
            stuff = int(stuff)
        return stuff
    except ValueError:
        return stuff
                    # PROMPT list!!! Whats up!
prompt = ["I,m gonna tell you your initials!",
          "Please enter your first name,",
          "Please enter your middle name or enter if you want to leave it blank,",
          "Now your last name,",
          "[name]>  ",
          "[$]>  ",
          "Fancy print dates!!!!",
          "Please enter a date in the form mm/dd/yyyy.",
          "[mm/dd/yyyy]>  ",
          "Please enter an alphanumeric phone number, I.E. 1 (800) pro gram, or 2035555555 or whatever.\n[phone number]>  ",
          "This program will read a selected text file, and tell you how many of different character types are in it.",
          "These are the files in your current directory.",
          "These are sub directories in your current directory.",
          "Would you like to read a file in the current directory, or navigate to a new directory to find a file? Please enter y to open o file here or c to choose a new directory.\n[y,c]>  ",
          "OK. Please enter the name of the directory.\n[dir name]>  ",
          "OK. Please type in the name of the file.\n[file name]>  ",
          "This program will separate and count consonants and vowels for any nonsense you type in!",
          "\nType something then press enter.\n[nonsense]>  "
         ]
                    # I added a few more this time!
insult = ["Numbers only please.", 
           """Either type "man" or "line", case sensitive, or enter to finish.""",
           "A name please... \nI mean its not that hard.",
           "It needs to be numbers in mm/dd/yyyy format,",
           "A US phone number please it should be within 10 to 11 characters depending on when you were born.",
           "Either y or c please.",
           "It needs to be a directory in the current directory or a fully qualified pathname.",
           "It needs to be a file in the current directory or a fully qualified pathname."
           ]
                    # Dictionary with language as keys and months as
                    # values. I don't really like it, but it was already
                    # made and I was behind. I gotta figure out a python
                    # database for all this language I keep insisting on
                    # using.
months = OrderedDict([('first', 'January'),
                      ('second', 'February'),
                      ('third', 'March'),
                      ('fourth', 'April'),
                      ('fifth', 'May'),
                      ('sixth', 'June'),
                      ('seventh', 'July'),
                      ('eight', 'August'),
                      ('ninth', 'September'),
                      ('tenth', 'October'),
                      ('eleventh', 'November'),
                      ('twelfth', 'December')])
         
                    # More of that only days this time!
days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
                    # a function whose awesomeness is completely ruined by
                    # the fact that IDLE is junk. I just realized today
                    # that one of my older ones that printed Fahrenheit
                    # and Celsius temperatures was also ruined by IDLE. I
                    # thin in that case it's because IDLE has even less
                    # support for ANSI escape codes than windows itself.
def clear_screen():
    print("\n" * 150)
                    # another unused just in my template.
def drange(low, high, step):
    _low = low
    while True:
        if _low > high:
            return
        yield _low
        _low += step

                    # Function to take the first element of each string
                    # in a list and make it uppercase. Then out put that
                    # list.
def upperFirst(_list):
    upperList=[]
    for i in _list:
        x = i[0].upper()
        upperList.append(x)
    return upperList
                    # Main function for example 1
                    # uses a list comprehension to fill a list with first
                    # middle and last names. I thought about arbitrary
                    # length names but it wasn't requested and i think
                    # this whole mess is complex enough as it is. Then
                    # filter the list to remove an empty entry for non
                    # killers who don't want to enter a middle
                    # name. Then apply the upperFirst function to get a
                    # new list then spit it out!
def ex_1():
    print(prompt[0])
    _names = [strLoop(prompt[x] + "\n" + prompt[4], insult[2]) for x in range(1,4)]
    _names = list(filter(None, _names))
    _initials = upperFirst(_names)
    _initials.append("")
    print("\n\n\n")
    sleep(1)
    print(". ".join(_initials))
                    # Main function for example 3
                    # In this homework assignment I decided to continue
                    # my tradition (mostly) of completely ignoring what
                    # the chapter is about and using different
                    # constructs. So for this one I decided to use
                    # Regular Expressions! I have always wanted to learn
                    # regexps mostly to try and understand a lot of the
                    # dense bash scripts and stuff I use. I also think
                    # they are a lot more powerful than the python
                    # builtins. The re module is distributed with python.
                    # This function takes an input in the form of
                    # nn/nn/nnnn as a date. Searches the string for the
                    # correct pattern if there's a match replace month
                    # number with month name and format the rest.
                    # otherwise insult and try again.
def ex_3():
    print(prompt[6])
    print("\n" + prompt[7] + "\n")
    match = re.compile(r'(\d\d)/(\d\d)/(\d\d\d\d)')
    while True:
        x = input(prompt[8])
        date = match.search(x)
        if date != None:
            _month,_day,_year = date.groups()
            print("\n")
            print(list(months.values())[int(_month)-1], " ", _day, ", ", _year, sep="")
            break
        else:
            print(insult[3])
                    # useful regexps for the following example. I put
                    # them out here to avoid lots of globals. They are
                    # match all numbers and letters, match all letters
                    # and match a phone number with or without a
                    # preceding 1.
alphaNum = re.compile(r'[a-zA-Z0-9]')
alpha = re.compile(r'[a-zA-Z]')
form = re.compile(r'(\d){,1}(\d\d\d)(\d\d\d)(\d\d\d\d)')
                    # This is an input loop that strips all extra
                    # characters from input phone number like ( and ) and
                    # -. Then it checks to see if there are the correct
                    # number of elements either 10 or 11 depending on the
                    # first 1.
def phoneLoop(_prompt,_insult):
    phoneNumber=[]
    while len(phoneNumber) not in [10,11]:
        phoneNumber=input(_prompt)
        phoneNumber="".join(alphaNum.findall(phoneNumber))
        if len(phoneNumber) in [10,11]: return phoneNumber
        else:
            print(_insult)
                    # this is my overly obfuscated exercise I think but
                    # maybe it makes sense. This function uses the string
                    # module to get a string with all the ASCII lowercase
                    # letters then splits it up into groups of three
                    # unless its 7 or 9 which are groups of four. Then
                    # assigns those groups as keys to a dict with the
                    # corresponding phone button as the value. Then defines
                    # a function for use in a regex replacement to check
                    # each letter in the phone number against the
                    # dictionary and replace it with the corresponding
                    # value. The regex replacement line lowercases the
                    # letters for searching.
def fonReplace(_string):
    a = [letters[(i-2)*3+(i>7):(i-2)*3+3+(i>=7)+(i==9)] for i in range(2,10)]
    phone = dict(zip(a,[str(x) for x in range(2,10)]))
    def replfun(iN):
        for i in phone.keys():
            if iN.group() in i:
                return phone[i]
    return re.sub(alpha, replfun, ("".join(re.findall(alphaNum, _string))).lower())
   
                    # The main function for example 5
                    # I think my goal on this one was to make it as
                    # un"pythonic" as possible, it was a liberating
                    # experience. I get that doing things the python way
                    # is supremely readable, but it also feels limiting
                    # and from everything I read on stack exchange etc,
                    # it seems like anything interesting or cool/tricky I
                    # want to do is unpythonic so... Anyways I tried to
                    # make a lispy list flattener with recursive functions but i
                    # couldn't get it to work. So this one calls the two
                    # preceding functions then adds the 1 if its missing,
                    # then adds formatting characters and prints the
                    # string. Ta daaaa. My favorite part is the list
                    # comprehension to add the formatting, because it's
                    # all side effects, the list it creates is three
                    # "None"s! Ha, take that Van Rossum.
def ex_5():
    while True:
        phoneNumber = phoneLoop(prompt[9], insult[4])
        phoneNumber = fonReplace(phoneNumber)
        formAt = form.search(phoneNumber)
        if not formAt.group(1):
            phoneNumber = "".join(list(formAt.groups("1")))
            formAt = form.search(phoneNumber)
        if formAt.group(1) == "1":
            phoneNumber = list(formAt.groups())
            [phoneNumber.insert(x,y) for x,y in zip(range(1,7,2),[" (",") ","-"])]
            sleep(1)
            print("\n\n", "".join(phoneNumber))
            break
        else:
            print(insult[4])
                    # This is the main for 8
                    # For this one I used the os module, hopefully this
                    # means that in the first part it will print all the
                    # files in the current directory and then all the
                    # directories in the current directory, then ask if
                    # you want to select a file from the current
                    # directory or go to a new one. Once a file is
                    # selected it opens it with with so it should be
                    # closed automagically, and reads it to a variable.
                    # Then it uses regexps to separate the characters
                    # into lists and then just print out the list length.
                    # "d" "u" "n" done. Oh there's also a whole lot of
                    # input error checking but I think it,s hearty. I'll
                    # check this in class to make sure it works on
                    # windows. Now for some more preaching. I wanted to
                    # have the directories and files print in different
                    # colors and maybe have a selection menu. But the
                    # idle shell has no support for ANSI escape codes,
                    # and I'm trying to stick with the modules bundled
                    # with python. So might I recommend a real shell?
                    # perhaps for in the future? If i recall correctly
                    # windows 10 cmd shell supports ANSI with some
                    # configuration. There's also PuTTy which is probably
                    # as close as you can get to a Unix terminal for
                    # windows. But! There's also powershell which comes
                    # with windows (I know can you believe it, it seems
                    # so sparse) which has the additional feature of
                    # having an object oriented command shell which I
                    # think could probably be pretty useful. Worth a try
                    # and powershell supports ANSI escape codes so we can
                    # have fun with colors and stuff! Hows that for a
                    # verbose comment.
def ex_8():
    print("\n\n", prompt[10])
    sleep(1)
    print("\n\n")
    cwd = os.getcwd()
    _yn = "c"
    while _yn in ["c", "C"]:
        for i in os.scandir(os.getcwd()):
            if i.is_file():
                print(i.name)
        print(prompt[11])
        sleep(1)
        print("\n\n")
        for i in os.scandir(os.getcwd()):
            if i.is_dir():
                print(i.name)
        print(prompt[12])
        sleep(1)
        print("\n\n")
        _yn = strictStrLoop(prompt[13], insult[5], ["y", "n", "c", "C"])
        if _yn in ["c", "C"]:
            while True:
                try:
                    os.chdir(input(prompt[14]))
                    break
                except FileNotFoundError:
                    print(insult[6])
    if _yn in ["y", "Y"]:
        while True:
            try:
                with open(input(prompt[15])) as faiul:
                    text = faiul.read()
                    break
            except FileNotFoundError:
                print(insult[7])
        print("The number of uppercase letters in the file is", len(re.findall(r'[A-Z]', text)))
        print("The number of lowercase letters in the file is", len(re.findall(r'[a-z]', text)))
        print("The number of decimal digits in the file are", len(re.findall(r'\d', text)))
        print("The number of white space characters (including \\n and \\r and so on) is", len(re.findall(r'\s', text)))
        os.chdir(cwd)

                    # Main 10
                    # More regexps! This one tells you to type in some
                    # random stuff, then regexps two lists one with just
                    # vowels one with just consonants and tells you how
                    # many.
def ex_10():
    print("\n\n", prompt[16])
    sleep(1)
    global stuff
    stuff = input(prompt[17])
    stuff = re.sub(r'[^a-zA-Z]', '', stuff)
    vow = re.findall(r'[aeiouAEIOU]', stuff)
    cons = list(re.sub(r'[aeiouAEIOU ]', '', stuff))
    print("\nThe vowels are,", ",".join(vow))
    print("and there are", len(vow), "of them.")
    print("\nThe consonants are,", ",".join(cons))
    print("and there were", len(cons), "of them.")

                    # Copy paste from other assignment
prawnz = ["\nPlease select which programming exercise you would like to run, or q to exit:\n[1,3,5,8,10 or q]>  "]  

                    # This is the main loop. It asks for input of the
                    # exercise number and then runs the corresponding
                    # function. It will continue looping until the letter
                    # "q" is entered.

def mainList():
    ex_num = 'foo' 
    while ex_num != 'q': 
        print("\n")
        ex_num = typeIn(prawnz[0])
        if ex_num == 1:
            ex_1()
        elif ex_num == 3:
            ex_3()
        elif ex_num == 5:
            ex_5()
        elif ex_num == 8:
            ex_8()
        elif ex_num == 10:
            ex_10()

                    # run that puppy!
mainList()

































                    # such a empty! No jokes for you!
